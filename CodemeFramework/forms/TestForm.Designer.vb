﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TestForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.date_added = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.section = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.position = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.note = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.date_added, Me.section, Me.position, Me.note})
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(667, 378)
        Me.DataGridView1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 384)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'date_added
        '
        Me.date_added.HeaderText = "Date"
        Me.date_added.Name = "date_added"
        '
        'section
        '
        Me.section.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.section.HeaderText = "Section"
        Me.section.Items.AddRange(New Object() {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"})
        Me.section.Name = "section"
        '
        'position
        '
        Me.position.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.position.HeaderText = "Position"
        Me.position.Items.AddRange(New Object() {"Pos 1", "Pos 2", "Pos 3", "Pos 4", "Pos 5"})
        Me.position.Name = "position"
        '
        'note
        '
        Me.note.HeaderText = "Note"
        Me.note.Name = "note"
        Me.note.Width = 400
        '
        'TestForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(667, 417)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "TestForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TestForm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents date_added As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents section As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents position As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents note As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
