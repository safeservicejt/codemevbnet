﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Windows.Forms
Imports System.Diagnostics
Imports System.IO
Imports System.Drawing
Imports CodemeFramework.includes
'Imports CodemeFramework.forms

Public Class PingForm

    Dim ipAddress As String = "10.220.3.10"

    Dim pingThread As Thread

    Dim canStart As Boolean = True

    Dim totalPing As Integer = 0

    Private Sub PingForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.Text = "Hi you!"

        Panel1.Hide()

        pingThread = New Thread(New ThreadStart(AddressOf pingToIP))

        pingThread.Start()

    End Sub

    Private Sub toNG()

        canStart = True

        Me.Invoke(New MethodInvoker(Sub()
                                        Me.BackColor = Color.Coral

                                        lbStatus.Text = "Ping NG"

                                        Me.Text = "Ping to " + ipAddress + " - NG (" + totalPing.ToString() + ")"


                                    End Sub))

        Thread.Sleep(1000)


    End Sub

    Private Sub toOK()

        canStart = False

        Me.Invoke(New MethodInvoker(Sub()
                                        Me.BackColor = Color.OliveDrab

                                        lbStatus.Text = "Ping OK"

                                        Me.Text = "Ping to " + ipAddress + " - OK (" + totalPing.ToString() + ")"


                                    End Sub))


    End Sub

    Private Sub pingToIP()

        While True
            If canStart = True And ipAddress.Length > 5 Then

                totalPing += 1

                Me.Invoke(New MethodInvoker(Sub()
                                                Me.Text = "Ping to " + ipAddress + " (" + totalPing.ToString() + ")"

                                                lbStatus.Text = "Pinging..."

                                            End Sub))

                Try
                    Networks.pingHost(ipAddress)

                    toOK()

                    'Me.Invoke(New MethodInvoker(Function() Me.Text = "Ping to " + ipAddress + " - OK (" + totalPing.ToString() + ")"))
                Catch ex As Exception

                    'MessageBox.Show(ex.Message)

                    toNG()

                    'Me.Invoke(New MethodInvoker(Function() Me.Text = "Ping to " + ipAddress + " - NG (" + totalPing.ToString() + ")"))
                End Try


            End If
        End While
    End Sub

    Private Sub txtIP_KeyUp(sender As Object, e As KeyEventArgs) Handles txtIP.KeyUp
        If e.KeyValue = Keys.Enter Then

            ipAddress = txtIP.Text.Trim()

            canStart = True

            totalPing = 0

            Panel1.Hide()

        End If
    End Sub

    Private Sub lbStatus_DoubleClick(sender As Object, e As EventArgs) Handles lbStatus.DoubleClick
        canStart = False

        totalPing = 0

        Panel1.Show()

    End Sub

    Private Sub PingForm_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        canStart = False

        totalPing = 0

        Panel1.Show()
    End Sub

    Private Sub PingForm_Leave(sender As Object, e As EventArgs) Handles MyBase.Leave

        canStart = False

        pingThread.Abort()
    End Sub
End Class