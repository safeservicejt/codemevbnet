Imports System.Collections.Generic
Imports System.Linq
Imports System.Threading
Imports System.Windows.Forms

NotInheritable Class Program
	Private Sub New()
	End Sub
	''' <summary>
	''' The main entry point for the application.
	''' </summary>
	<STAThread> _
	Friend Shared Sub Main()
		Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)

        Try
            Application.Run(New TestForm())
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

	End Sub
End Class
