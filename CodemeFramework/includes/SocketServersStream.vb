Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Net.Sockets

Namespace includes
	Class SocketServersStream
		Private Const BUFFER_SIZE As Integer = 1024
		Private Const PORT_NUMBER As Integer = 9999

		Shared encoding As New ASCIIEncoding()

		Public Shared Sub Start()
			Try
				Dim address As IPAddress = IPAddress.Parse("127.0.0.1")

				Dim listener As New TcpListener(address, PORT_NUMBER)

				' 1. listen
				listener.Start()

				Console.WriteLine("Server started on " & Convert.ToString(listener.LocalEndpoint))
				Console.WriteLine("Waiting for a connection...")

				Dim socket As Socket = listener.AcceptSocket()
				Console.WriteLine("Connection received from " & Convert.ToString(socket.RemoteEndPoint))

                Dim stream As NetworkStream = New NetworkStream(socket)
                Dim reader As StreamReader = New StreamReader(stream)
                Dim writer As StreamWriter = New StreamWriter(stream)
				writer.AutoFlush = True

				While True
					' 2. receive
					Dim str As String = reader.ReadLine()
					If str.ToUpper() = "EXIT" Then
						writer.WriteLine("bye")
						Exit While
					End If
					' 3. send
					writer.WriteLine("Hello " & str)
				End While
				' 4. close
				stream.Close()
				socket.Close()
				listener.[Stop]()
			Catch ex As Exception
				Console.WriteLine("Error: " & Convert.ToString(ex))
			End Try
			Console.Read()
		End Sub
	End Class
End Namespace
