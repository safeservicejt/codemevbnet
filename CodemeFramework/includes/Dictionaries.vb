﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Data
Imports System.Windows.Forms
Imports System.Threading
Imports System.Collections
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports CodemeFramework.includes


Public Class Dictionaries

    Public Shared Sub enableControl(dic As Dictionary(Of String, String), keyName As String, textCompare As String, ctrl As Control)
        If dic.ContainsKey(keyName) = False Then
            ctrl.Enabled = True
        ElseIf dic.Item(keyName) = textCompare Then
            ctrl.Enabled = True
        End If
    End Sub

    Public Shared Sub disableControl(dic As Dictionary(Of String, String), keyName As String, textCompare As String, ctrl As Control)
        If dic.ContainsKey(keyName) = False Then
            ctrl.Enabled = False
        ElseIf dic.Item(keyName) = textCompare Then
            ctrl.Enabled = False
        End If
    End Sub

    Public Shared Sub writeToIni(ByVal fileName As String, ByVal inputArray As Dictionary(Of String, String))
        'Dictionary<string, string> configs = new Dictionary<string, string>();
        'configs["test"] = "ok";
        'configs["ab"] = "fdgdgsdf";
        'configs["12"] = "234324234";
        'IniFile.makeFromArray("filetest", configs);
        Dim savePath As String = (App.rootPath + "\" + fileName + ".ini")
        Files.make(savePath, "")
        Dim MyIni As IniFile = New IniFile(savePath)
        For Each theKey As String In inputArray.Keys
            MyIni.Write(theKey, inputArray(theKey), "General")
        Next
    End Sub

    Public Shared Sub addToIni(ByVal fileName As String, ByVal inputArray As Dictionary(Of String, String))
        'Dictionary<string, string> configs = new Dictionary<string, string>();
        'configs["test"] = "ok";
        'configs["ab"] = "fdgdgsdf";
        'configs["12"] = "234324234";
        'IniFile.makeFromArray("filetest", configs);
        Dim savePath As String = (App.rootPath + "\" + fileName + ".ini")
        Dim MyIni As IniFile = New IniFile(savePath)
        For Each theKey As String In inputArray.Keys
            MyIni.Write(theKey, inputArray(theKey), "General")
        Next
    End Sub


    Public Shared Function getFromIni(ByVal fileName As String) As Dictionary(Of String, String)
        'Dictionary<string, string> configs = new Dictionary<string, string>();
        'configs = Cache.getFromIni("filetest");
        'MessageBox.Show(configs["ab"]);
        If (fileName.Length = 0) Then
            Throw New Exception("File name must more than 1 character.")
        End If

        Dim savePath As String = (App.rootPath + "\" + fileName + ".ini")
        If Not File.Exists(savePath) Then
            Throw New Exception(("The file: " + (fileName + (" not exist in: " + savePath))))
        End If

        Dim dict As New Dictionary(Of String, String)()

        Dim fileData As String() = File.ReadAllLines(savePath)

        Dim keyName As String = ""

        Dim keyVal As String = ""


        For i As Integer = 0 To fileData.Length
            Try
                If fileData(i).Substring(0, 1) <> "#" And fileData(i).Substring(0, 1) <> "'" Then
                    keyName = fileData(i).Substring(0, fileData(i).IndexOf("="))

                    keyVal = fileData(i).Substring(fileData(i).IndexOf("=") + 1)

                    dict.Add(keyName, keyVal)
                End If

            Catch ex As Exception

            End Try

        Next

        Return dict
    End Function
End Class
