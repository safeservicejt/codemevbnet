Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Threading
Imports System.Diagnostics
Imports System.Net
Imports System.Net.Sockets
Imports System.Net.NetworkInformation
Imports System.Web.Services.Protocols
Imports System.Text.RegularExpressions
Imports System.Runtime.InteropServices
Imports Microsoft.Win32
Imports System.Windows.Forms
Imports CodemeFramework.includes


Namespace includes
    Class Systems
        'CodemeSystems.runCommand(@"mkdir testst");
        'Systems.temp.Add("username", "testfdemo")

        'MessageBox.Show(Systems.temp.Item("username"))

        'List form 
        'Systems.loadingForm = New LoadingForm()
        ''Dim loadingForm As LoadingForm = New LoadingForm()
        'Systems.loadingForm.Show()
        'Public Shared loadingForm As New LoadingForm()

        'Public Shared mainForm As New MainForm()


        'End list form

        Public Shared errorStr As String = ""

        Public Shared configs As New Dictionary(Of String, String)()

        Public Shared programdata As New Dictionary(Of String, String)()

        Public Shared permissions As New Dictionary(Of String, String)()

        Public Shared userdata As New Dictionary(Of String, String)()

        Public Shared user_permissions As New Dictionary(Of String, String)()

        Public Shared temp As New Dictionary(Of String, String)()

        Public Shared Function getMessage() As String
            Return errorStr
        End Function

        Public Shared Sub writeMessage(Optional inputStr As String = "")
            errorStr = inputStr
        End Sub

        Public Shared Sub showError()
            'Dim err As New ErrorForm()

            'err.Show()
        End Sub


        Public Shared Function getMachineName() As String
            'System.Windows.Forms.SystemInformation.ComputerName
            'Environment.MachineName 

            Dim ComputerName As String
            ComputerName = System.Net.Dns.GetHostName
            Return ComputerName
        End Function


        Public Shared Sub ping(inputStr As String)
            'If My.Computer.Network.Ping("198.01.01.01") Then
            '    MsgBox("Server pinged successfully.")
            'Else
            '    MsgBox("Ping request timed out.")
            'End If

            If (inputStr.Length < 5) Then
                Throw New Exception("Address not valid.")
            End If

            Try
                If (My.Computer.Network.Ping(inputStr) = False) Then
                    Throw New Exception("Failed ping to " + inputStr)
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Sub

        Public Shared Function getCommandOutput(inputCommand As String) As String
            Dim proc As New Process()
            Dim oStartInfo As New ProcessStartInfo("cmd.exe", "/c " + inputCommand)
            oStartInfo.UseShellExecute = False
            oStartInfo.RedirectStandardOutput = True
            oStartInfo.CreateNoWindow = True
            proc.StartInfo = oStartInfo
            proc.Start()

            Dim sOutput As String

            Using oStreamReader As System.IO.StreamReader = proc.StandardOutput
                sOutput = oStreamReader.ReadToEnd()
            End Using

            proc.Dispose()

            Return sOutput
        End Function

        Public Shared Function isStartup(Optional appName As String = "") As Boolean
            If appName = "" Then
                appName = "CodeProgram"
            End If

            Dim rkApp As RegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)

            Dim result As Boolean = False

            If rkApp.GetValue(appName) Is Nothing Then
                ' The value doesn't exist, the application is not set to run at startup
                result = False
            Else
                ' The value exists, the application is set to run at startup
                result = True
            End If

            Return result
        End Function

        Public Shared Sub setStartup(Optional appName As String = "")
            Dim rkApp As RegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)

            If rkApp.GetValue(appName) Is Nothing Then
                ' The value doesn't exist, the application is not set to run at startup
                rkApp.SetValue(appName, Application.ExecutablePath)
            End If
        End Sub

        Public Shared Sub unsetStartup(Optional appName As String = "")
            Dim rkApp As RegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)

            If rkApp.GetValue(appName) IsNot Nothing Then
                ' The value existed, the application is set to run at startup
                rkApp.DeleteValue(appName, False)
            End If
        End Sub

        Public Shared Sub runCommand(Optional anyCommand As String = "")
            Dim proc1 As ProcessStartInfo = New ProcessStartInfo()

            proc1.UseShellExecute = True

            proc1.WorkingDirectory = "C:\Windows\System32"

            proc1.FileName = "C:\Windows\System32\cmd.exe"
            'proc1.Verb = "runas";
            proc1.Arguments = "/c cd\ && " & anyCommand
            proc1.WindowStyle = ProcessWindowStyle.Hidden
            Process.Start(proc1)
        End Sub

        Public Shared Function getAllLocalIp() As String()

            Dim listIP As New List(Of String)

            Dim host As IPHostEntry
            host = Dns.GetHostEntry(Dns.GetHostName())
            For Each ip As IPAddress In host.AddressList

                If Regex.IsMatch(ip.ToString(), "\d+\.\d+\.\d+\.\d+") Then
                    listIP.Add(ip.ToString())
                End If
                'If ip.AddressFamily = AddressFamily.InterNetwork Then
                '    listIP.Add(ip.ToString())
                '    'Return ip.ToString()
                'End If
            Next

            Return listIP.ToArray()

        End Function

        Public Shared Function getFirstLocalIp() As String
            Dim host As IPHostEntry
            host = Dns.GetHostEntry(Dns.GetHostName())
            For Each ip As IPAddress In host.AddressList

                MessageBox.Show(ip.ToString())
                If ip.AddressFamily = AddressFamily.InterNetwork Then
                    'Return ip.ToString()
                End If
            Next


            Return "127.0.0.1"
        End Function

        Public Shared Function getIpFromHostname(strHostName As String) As String

            Dim strIPAddress As String

            strHostName = System.Net.Dns.GetHostName()

            strIPAddress = System.Net.Dns.GetHostEntry(strHostName).AddressList(0).ToString()

            Return strIPAddress

        End Function

        Public Shared Function getOSName() As String
            Dim result As String = My.Computer.Info.OSFullName

            Return result
        End Function

        Public Shared Function getOSVersion() As String
            Dim result As String = My.Computer.Info.OSVersion

            Return result
        End Function

        Public Shared Function getOSPlatform() As String
            Dim result As String = My.Computer.Info.OSPlatform

            Return result
        End Function

        Public Shared Function getListUsersOnMachine() As String()
            Dim listUsers As String() = New String() {}

            Dim i As Integer = 0

            Dim userskey As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList")
            For Each keyname As String In userskey.GetSubKeyNames()
                Using key As RegistryKey = userskey.OpenSubKey(keyname)
                    Dim userpath As String = DirectCast(key.GetValue("ProfileImagePath"), String)
                    Dim username As String = System.IO.Path.GetFileNameWithoutExtension(userpath)
                   
                    listUsers(i) = username
                End Using

                i += 1
            Next

            Return listUsers
        End Function

        Public Shared Function getTextFromClipboard() As String
            Dim result As String = My.Computer.Clipboard.GetText()

            Return result
        End Function

        Public Shared Sub clearClipboard(inputStr As String)
            My.Computer.Clipboard.Clear()
        End Sub

        Public Shared Function isConnected() As Boolean
            Dim result As Boolean = My.Computer.Network.IsAvailable

            Return result
        End Function

        Public Shared Function getNetWorkHostNames() As String()
            Dim result As String() = {}
            Dim i As Integer = 0

            Dim netUtility As New Process()

            netUtility.StartInfo.FileName = "net.exe"

            netUtility.StartInfo.CreateNoWindow = True

            netUtility.StartInfo.Arguments = "view"

            netUtility.StartInfo.RedirectStandardOutput = True

            netUtility.StartInfo.UseShellExecute = False

            netUtility.StartInfo.RedirectStandardError = True

            netUtility.Start()

            Dim line As String = ""

            Dim streamReader As New StreamReader(netUtility.StandardOutput.BaseStream, netUtility.StandardOutput.CurrentEncoding)

            While (InlineAssignHelper(line, streamReader.ReadLine())) IsNot Nothing
                If line.StartsWith("\") Then
                    result(i) = line

                    i += 1
                End If
            End While

            streamReader.Close()
            netUtility.WaitForExit(1000)

            Return result
        End Function

        Public Shared Sub killProcessByName(inputName As String)
            For Each process__1 As Process In Process.GetProcessesByName(inputName)

                'Get process descriptions
                'process__1.MainModule.FileVersionInfo.FileDescription

                process__1.Kill()
            Next
        End Sub

        Public Shared Sub runFile(filePath As String)
            Process.Start(filePath)
        End Sub


        Public Sub urlShortcutToDesktop(linkName As String, linkUrl As String)
            Dim deskDir As String = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)

            Using writer As New StreamWriter(deskDir & "\" & linkName & ".url")
                writer.WriteLine("[InternetShortcut]")
                writer.WriteLine("URL=" & linkUrl)
                writer.Flush()
            End Using
        End Sub

        Public Sub appShortcutToPath(linkName As String, Optional targetPath As String = "")
            Dim deskDir As String = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)

            If targetPath.Length > 2 Then
                deskDir = targetPath
            End If

            Using writer As New StreamWriter(deskDir & "\" & linkName & ".url")
                Dim app As String = System.Reflection.Assembly.GetExecutingAssembly().Location
                writer.WriteLine("[InternetShortcut]")
                writer.WriteLine("URL=file:///" & app)
                writer.WriteLine("IconIndex=0")
                Dim icon As String = app.Replace("\"c, "/"c)
                writer.WriteLine("IconFile=" & icon)
                writer.Flush()
            End Using
        End Sub
        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function

    End Class
End Namespace
