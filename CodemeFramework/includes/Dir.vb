Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.IO.Compression
Imports System.Windows.Forms

Namespace includes
	Class Dir

		Public Function selectFolder() As String
			Dim sSelectedFolder As String = ""

			Dim fbd As New FolderBrowserDialog()
			'fbd.Description = "Custom Description"; //not mandatory

			If fbd.ShowDialog() = DialogResult.OK Then
				sSelectedFolder = fbd.SelectedPath
			Else
				sSelectedFolder = String.Empty
			End If

			Return sSelectedFolder
		End Function

        'Public Shared Sub compressZIP(startPath As String, targetFile As String)
        '	Try
        '		'string startPath = @"c:\example\start";
        '		'string zipPath = @"c:\example\result.zip";
        '		ZipFile.CreateFromDirectory(startPath, targetFile)
        '	Catch ex As Exception
        '		Throw New Exception(ex.Message)
        '	End Try
        'End Sub

        'Public Shared Sub extractZIP(filePath As String, targetPath As String)

        '	'string zipPath = @"c:\example\result.zip";
        '	'string extractPath = @"c:\example\extract";
        '	Try
        '		ZipFile.ExtractToDirectory(filePath, targetPath)
        '	Catch ex As Exception
        '		Throw New Exception(ex.Message)
        '	End Try
        'End Sub

		Public Shared Sub CloneDirectory(root As String, dest As String)
            For Each directory__1 As String In Directory.GetDirectories(root)
                Dim dirName As String = Path.GetFileName(directory__1)
                If Not Directory.Exists(Path.Combine(dest, dirName)) Then
                    Directory.CreateDirectory(Path.Combine(dest, dirName))
                End If
                CloneDirectory(directory__1, Path.Combine(dest, dirName))
            Next

            For Each file__2 As String In Directory.GetFiles(root)
                File.Copy(file__2, Path.Combine(dest, Path.GetFileName(file__2)), True)
            Next
		End Sub

		Public Shared Sub delete(FolderName As String)
			Dim dir As New DirectoryInfo(FolderName)

			For Each fi As FileInfo In dir.GetFiles()
				fi.Delete()
			Next

			For Each di As DirectoryInfo In dir.GetDirectories()
				delete(di.FullName)
				di.Delete()
			Next
		End Sub

		Public Shared Sub move(sourcePath As String, targetPath As String)

			' To move an entire directory. To programmatically modify or combine
			' path strings, use the System.IO.Path class.
			System.IO.Directory.Move(sourcePath, targetPath)
		End Sub

		Public Shared Sub make(Optional inputData As String = "")
			inputData = inputData.Replace("{PATH}", App.rootPath)

			System.IO.Directory.CreateDirectory(inputData)
		End Sub


		Public Shared Function exists(Optional inputData As String = "") As Boolean
			Dim status As Boolean = False

			If Directory.Exists(inputData) AndAlso empty(inputData) Then
				status = True
			End If

			Return status
		End Function

		Public Shared Function DirSearch(sDir As String, Optional pattern As String = "*.*") As String()
			Dim result As String() = {}

			'int total = 0;

			result = Directory.GetFiles(sDir, pattern, SearchOption.AllDirectories)

			'try
			'{
			'    foreach (string d in Directory.GetDirectories(sDir))
			'    {
			'        foreach (string f in Directory.GetFiles(d, pattern))
			'        {

			'            result[total] = f;

			'            total++;

			'        }
			'        DirSearch(d);
			'    }
			'}
			'catch (Exception ex)
			'{

			'}

			Return result
		End Function

		Public Shared Function listFiles(Optional filePath As String = "", Optional pattern As String = "*.*") As String()
			'string[] files = Directory.GetFiles("C:\\", "*.dll");
			Dim result As String() = {}

			If File.Exists(filePath) Then
				result(0) = filePath
			ElseIf Directory.Exists(filePath) Then
				result = Directory.GetFiles(filePath, pattern)
			End If

			Return result
		End Function

		Public Shared Function listDirs(Optional filePath As String = "") As String()
			Dim result As String() = {}

			If File.Exists(filePath) Then
				result(0) = filePath
			ElseIf Directory.Exists(filePath) Then
				result = Directory.GetDirectories(filePath)
			End If

			Return result
		End Function

		Public Shared Function empty(Optional inputData As String = "") As Boolean
			Dim status As Boolean = False

			If Directory.GetFiles(inputData).Length = 0 AndAlso System.IO.Directory.GetFiles(inputData, "*", SearchOption.AllDirectories).Length = 0 Then
				status = True
			End If

			Return status
		End Function



	End Class
End Namespace
