Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Xml.Serialization
Imports System.Windows.Forms

Namespace includes
	Public Class Files

		'File transfer
		Public fileName As String = ""
		Public fileSize As Integer = 0
		Public fileContent As String = ""
		'File transfer


		Public Shared Sub sendFile(Optional IP As String = "", Optional Port As Integer = 68686, Optional filePath As String = "", Optional savePath As String = "")
			If IP = "" OrElse Port = 0 Then
				Throw New Exception("IP and Port " & IP & ":" & Port & " is not valid.")
			End If

			If Not File.Exists(filePath) Then
				Throw New Exception("The file: " & filePath & " not exists.")
			End If

			If savePath = "" Then
				Throw New Exception("Save path: " & savePath & " is not valid.")
			End If

			Try
				Dim fileTransfer As New FileTransfer()
				fileTransfer.Name = basename(filePath)
				fileTransfer.savePath = savePath

				fileTransfer.Content = System.Convert.ToBase64String(File.ReadAllBytes(filePath))

				Dim x As New System.Xml.Serialization.XmlSerializer(fileTransfer.[GetType]())

				Dim client As New TcpClient()
				client.Connect(IPAddress.Parse(IP), Port)
				Dim stream As Stream = client.GetStream()
				x.Serialize(stream, fileTransfer)

				client.Close()
			Catch ex As Exception
				Throw New Exception(ex.Message)
			End Try

		End Sub

		Public Shared Sub getFile(Optional Port As Integer = 68686)
			Try
				Dim list As TcpListener
				Dim port1 As Int32 = Port

				list = New TcpListener(IPAddress.Any, port1)
				list.Start()

				Dim client As TcpClient = list.AcceptTcpClient()
				'Console.WriteLine("Client trying to connect");

				Dim stream As Stream = client.GetStream()
				Dim mySerializer As New XmlSerializer(GetType(FileTransfer))
				Dim myObject As FileTransfer = DirectCast(mySerializer.Deserialize(stream), FileTransfer)
				'Console.WriteLine("name: " + myObject.Name);

				list.[Stop]()
				client.Close()

				Dim path__1 As String = Path.GetFullPath(myObject.savePath)


					'if (myObject.savePath.IndexOf("\\") == -1)
					'{
					'    //Save file to local computer
					'    string path = Path.GetFullPath(myObject.savePath);

					'    make(myObject.Name, myObject.Content);
					'}
					'else
					'{
					'    //Copy file to network computer
					'    make(@"D:\" + myObject.Name, myObject.Content);

					'    copy(@"D:\" + myObject.Name,@myObject.savePath + myObject.Name);
					'}
				make(myObject.Name, myObject.Content)
			Catch ex As Exception
				Throw New Exception(ex.Message)
			End Try

		End Sub


		Public Shared Sub openSelectFile()
			Dim openFileDialog1 As New OpenFileDialog()

			openFileDialog1.InitialDirectory = "c:\"
			openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
			openFileDialog1.FilterIndex = 2
			openFileDialog1.RestoreDirectory = True
			openFileDialog1.Multiselect = False

			If openFileDialog1.ShowDialog() = DialogResult.OK Then
				Try
					MessageBox.Show(openFileDialog1.FileName)
				Catch ex As Exception
					MessageBox.Show("Error: Could not read file from disk. Original error: " & ex.Message)
				End Try
			End If

			'FolderBrowserDialog fbd = new FolderBrowserDialog();
			'''/fbd.Description = "Custom Description"; //not mandatory

			'if (fbd.ShowDialog() == DialogResult.OK)
			'    sSelectedFolder = fbd.SelectedPath;
			'else
			'    sSelectedFolder = string.Empty;    
		End Sub

		Public Sub saveSelectFile()
			Dim saveFileDialog1 As New SaveFileDialog()

			saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
			saveFileDialog1.FilterIndex = 2
			saveFileDialog1.RestoreDirectory = True

			If saveFileDialog1.ShowDialog() = DialogResult.OK Then
				Using sw As New StreamWriter(saveFileDialog1.FileName)
					sw.WriteLine("Hello World!")
				End Using
			End If
		End Sub
		Public Shared Function basename(Optional filePath As String = "") As String
			Dim name As String = Path.GetFileName(filePath)

			Return name
		End Function

		Public Shared Function extension(Optional filePath As String = "") As String
			Dim name As String = Path.GetExtension(filePath)

			Return name
		End Function

		Public Shared Sub copy(Optional strFrom As String = "", Optional strTo As String = "")
            Dim fileNameFrom As String = Path.GetFileName(strFrom)
			'var fileNameTo = Path.GetFileName(strTo);

			'var local = Path.Combine(@strFrom, fileNameFrom);

            Dim local As String = strFrom
            Dim remote As String = Path.Combine(strTo, fileNameFrom)

			Try
				File.Copy(local, remote, True)
			Catch ex As Exception
				Throw New Exception(ex.Message)
			End Try
		End Sub

		Public Shared Sub move(Optional strFrom As String = "", Optional strTo As String = "")
            Dim fileNameFrom As String = Path.GetFileName(strFrom)

            Dim local As String = Path.Combine(strFrom, fileNameFrom)

			File.Move(local, strTo)
		End Sub

		Public Shared Sub remove(Optional strTo As String = "")
            Dim fileNameTo As String = Path.GetFileName(strTo)

            Dim remote As String = Path.Combine(strTo, fileNameTo)

			File.Delete(remote)
		End Sub

		Public Shared Sub open(Optional filePath As String = "")
			System.Diagnostics.Process.Start(filePath)
		End Sub

		Public Shared Function getContents(Optional filePath As String = "") As String
			Dim result As String = ""

			If System.IO.File.Exists(filePath) Then
				result = System.IO.File.ReadAllText(filePath)
			End If

			Return result
		End Function

		Public Shared Function getLines(Optional filePath As String = "") As String()
			Dim result As String() = {}

			If System.IO.File.Exists(filePath) Then
				result = System.IO.File.ReadAllLines(filePath)
			End If

			Return result
		End Function

		Public Shared Function getPath(Optional filePath As String = "") As String
			Dim result As String = ""

			result = System.IO.Path.GetDirectoryName(filePath)

			Return result
		End Function

		Public Shared Sub make(Optional filePath As String = "", Optional fileData As String = "")
			'Dir dr = new Dir();

			filePath = filePath.Replace("{PATH}", App.rootPath)

			Try
				Dim dirPath As String = getPath(filePath)

				If Not Dir.exists(dirPath) Then
					Dir.make(dirPath)
				End If

				Using fs As FileStream = System.IO.File.Create(filePath)
                    Dim info As Byte() = New UTF8Encoding(True).GetBytes(fileData)
					' Add some information to the file.
					fs.Write(info, 0, info.Length)

					fs.Close()
				End Using
			Catch ex As Exception
				Throw New Exception("Error", ex)
			End Try

		End Sub

		Public Shared Sub delete(Optional filePath As String = "")
			If System.IO.File.Exists(filePath) Then
				System.IO.File.Delete(filePath)
			End If
		End Sub
	End Class
End Namespace
