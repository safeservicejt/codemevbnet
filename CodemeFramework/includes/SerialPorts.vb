Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Windows.Forms
Imports System.Threading
Imports System.IO.Ports

'http://www.c-sharpcorner.com/uploadfile/eclipsed4utoo/communicating-with-serial-port-in-c-sharp/

Namespace includes
	Public Class SerialPorts

		Shared _continue As Boolean
		Shared _serialPort As SerialPort

		Shared inputData As String = ""

		Shared outputData As String = ""

		Shared readThread As Thread

        Public Sub connect(Optional inPortname As String = "", Optional inBaudRate As String = "", Optional inParity As String = "", Optional inDataBit As String = "", Optional inStopBit As String = "", Optional inHandShake As String = "")
            'StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
            'readThread = new Thread(Read);

            ' Create a new SerialPort object with default settings.
            _serialPort = New SerialPort()

            ' Allow the user to set the appropriate properties.
            If inPortname = "" Then
                Throw New Exception("Portname invalid")
            End If

            If Not inList(inPortname) Then
                Throw New Exception(inPortname & " not found in your system. Check it again!")
            End If

            _serialPort = New SerialPort(inPortname)

            ' Allow the user to set the appropriate properties.

            If inBaudRate <> "" Then
                _serialPort.BaudRate = Integer.Parse(inBaudRate)
            End If

            If inParity <> "" Then
                _serialPort.Parity = CType([Enum].Parse(GetType(Parity), inParity, True), Parity)
            End If

            If inStopBit <> "" Then
                _serialPort.StopBits = CType([Enum].Parse(GetType(StopBits), inStopBit, True), StopBits)
            End If

            If inHandShake <> "" Then
                _serialPort.Handshake = CType([Enum].Parse(GetType(Handshake), inHandShake, True), Handshake)
            End If

            ' Set the read/write timeouts
            _serialPort.ReadTimeout = 500
            _serialPort.WriteTimeout = 500

            Try
                _serialPort.Open()
            Catch ex As Exception
                _serialPort.Close()

                '_serialPort.Dispose();

                connect(inPortname, inBaudRate, inParity, inDataBit, inStopBit, inHandShake)
            End Try




        End Sub

		Public Sub disConnect()
			_serialPort.Close()

			_serialPort.Dispose()
		End Sub

		Public Sub Dispose()
			'readThread.Join();

			_serialPort.Close()

			_serialPort.Dispose()
		End Sub

		Public Function Read() As String
			inputData = _serialPort.ReadLine()

			Return inputData
		End Function

		Public Function checkCurrentPortStatus() As String
			Dim result As String = ""

			If Not inList(_serialPort.PortName) Then
				result = "NOTFOUND"
			ElseIf Not _serialPort.IsOpen Then
				result = "CLOSED"
			ElseIf _serialPort.IsOpen Then
				result = "OPENED"
			End If

			Return result
		End Function

		Public Function checkPortStatus(Optional inPortname As String = "", Optional inBaudRate As String = "", Optional inParity As String = "", Optional inDataBit As String = "", Optional inStopBit As String = "", Optional inHandShake As String = "") As String
			Dim result As String = ""

			inPortname = inPortname.ToUpper()

			If Not inList(inPortname) Then
				result = "NOTFOUND"
			Else
				Dim _checkPort As New SerialPort(inPortname)

				' Allow the user to set the appropriate properties.

				If inBaudRate <> "" Then
					_checkPort.BaudRate = Integer.Parse(inBaudRate)
				End If

				If inParity <> "" Then
					_checkPort.Parity = CType([Enum].Parse(GetType(Parity), inParity, True), Parity)
				End If

				If inStopBit <> "" Then
					_checkPort.StopBits = CType([Enum].Parse(GetType(StopBits), inStopBit, True), StopBits)
				End If

				If inHandShake <> "" Then
					_checkPort.Handshake = CType([Enum].Parse(GetType(Handshake), inHandShake, True), Handshake)
				End If

				' Set the read/write timeouts
				_checkPort.ReadTimeout = 500
				_checkPort.WriteTimeout = 500

				If Not _checkPort.IsOpen Then
					result = "CLOSED"
				ElseIf _checkPort.IsOpen Then
					result = "OPENED"
				End If

				_checkPort.Dispose()
			End If

			Return result
		End Function

		Public Function getPortName() As String
			Return _serialPort.PortName
		End Function


		Public Sub Send(Optional inputStr As String = "")
			Try
				_serialPort.WriteLine(inputStr)
			Catch ex As Exception
				Throw New Exception(ex.Message)
			End Try
		End Sub

		Public Function listPorts() As String()
			Dim ports As String() = SerialPort.GetPortNames()

			Return ports
		End Function

		Public Function inList(Optional inputPortName As String = "") As Boolean
			Dim portNames As String() = SerialPort.GetPortNames()

			If portNames.Contains(inputPortName) Then
				Return True
			End If

			Return False
		End Function



	End Class
End Namespace
