Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.Reflection
Imports System.Runtime.InteropServices

'
'
'    Open the INI file in one of the 3 following ways:
'
'    // Creates or loads an INI file in the same directory as your executable
'    // named EXE.ini (where EXE is the name of your executable)
'    var MyIni = new IniFile();
'
'    // Or specify a specific name in the current dir
'    var MyIni = new IniFile("Settings.ini");
'
'    // Or specify a specific name in a specific dir
'    var MyIni = new IniFile(@"C:\Settings.ini");
'    You can write some values like so:
'
'    MyIni.Write("DefaultVolume", "100");
'    MyIni.Write("HomePage", "http://www.google.com");
'    To create a file like this:
'
'    [MyProg]
'    DefaultVolume=100
'    HomePage=http://www.google.com
'    To read the values out of the INI file:
'
'    var DefaultVolume = IniFile.Read("DefaultVolume");
'    var HomePage = IniFile.Read("HomePage");
'    Optionally, you can set [Section]'s:
'
'    MyIni.Write("DefaultVolume", "100", "Audio");
'    MyIni.Write("HomePage", "http://www.google.com", "Web");
'    To create a file like this:
'
'    [Audio]
'    DefaultVolume=100
'
'    [Web]
'    HomePage=http://www.google.com
'    You can also check for the existence of a key like so:
'
'    if(!MyIni.KeyExists("DefaultVolume", "Audio"))
'    {
'        MyIni.Write("DefaultVolume", "100", "Audio");
'    }
'    You can delete a key like so:
'
'    MyIni.DeleteKey("DefaultVolume", "Audio");
'    You can also delete a whole section (including all keys) like so:
'
'    MyIni.DeleteSection("Web");
'    Please feel free to comment with any improvements!
'
'    var DefaultVolume = MyIni.Read("DefaultVolume", "Audio");
'
'    textBox1.Text = DefaultVolume;
' 


Namespace includes
	Class IniFile
		' revision 11
		Private Path As String
		Private EXE As String = Assembly.GetExecutingAssembly().GetName().Name

		<DllImport("kernel32", CharSet := CharSet.Unicode)> _
		Private Shared Function WritePrivateProfileString(Section As String, Key As String, Value As String, FilePath As String) As Long
		End Function

		<DllImport("kernel32", CharSet := CharSet.Unicode)> _
		Private Shared Function GetPrivateProfileString(Section As String, Key As String, [Default] As String, RetVal As StringBuilder, Size As Integer, FilePath As String) As Integer
		End Function

		Public Sub New(Optional IniPath As String = Nothing)
			Path = New FileInfo(If(IniPath, EXE & ".ini")).FullName.ToString()
		End Sub

		Public Function Read(Key As String, Optional Section As String = Nothing) As String
            Dim RetVal As StringBuilder = New StringBuilder(255)
			GetPrivateProfileString(If(Section, EXE), Key, "", RetVal, 255, Path)
			Return RetVal.ToString()
		End Function

		Public Sub Write(Key As String, Value As String, Optional Section As String = Nothing)
			WritePrivateProfileString(If(Section, EXE), Key, Value, Path)
		End Sub

		Public Sub DeleteKey(Key As String, Optional Section As String = Nothing)
			Write(Key, Nothing, If(Section, EXE))
		End Sub

		Public Sub DeleteSection(Optional Section As String = Nothing)
			Write(Nothing, Nothing, If(Section, EXE))
		End Sub

		Public Function KeyExists(Key As String, Optional Section As String = Nothing) As Boolean
			Return Read(Key, Section).Length > 0
		End Function



	End Class
End Namespace
