﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Threading
Imports System.Diagnostics
Imports System.Net
Imports System.Net.Sockets
Imports System.Net.NetworkInformation
Imports System.Web.Services.Protocols
Imports System.Runtime.InteropServices
Imports Microsoft.Win32
Imports System.Windows.Forms
Imports CodemeFramework.includes



Public Class Sounds

    Public Shared Sub playFile(inputAudioPath As String, Optional playLoop As Boolean = False)
        'C:\Waterfall.wav
        Try
            If playLoop = True Then
                My.Computer.Audio.Play(inputAudioPath, AudioPlayMode.BackgroundLoop)
            Else
                My.Computer.Audio.Play(inputAudioPath, AudioPlayMode.WaitToComplete)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Sub

    Public Shared Sub playFileFromList(inputAudioList As String(), Optional numberLoop As Integer = 1)
        'C:\Waterfall.wav

        For i As Integer = 1 To numberLoop
            For Each fileAudio As String In inputAudioList
                Try
                    My.Computer.Audio.Play(fileAudio, AudioPlayMode.WaitToComplete)
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            Next
        Next

    End Sub

End Class
