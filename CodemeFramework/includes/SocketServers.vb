Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Runtime.Serialization.Formatters.Binary

Namespace includes
	Public Class SocketServers
		Private Const BUFFER_SIZE As Integer = 1024
		Private Const PORT_NUMBER As Integer = 9999

		Shared encoding As New ASCIIEncoding()

		Public Shared Sub Start()
			Try
				Dim address As IPAddress = IPAddress.Parse("127.0.0.1")

				Dim listener As New TcpListener(address, PORT_NUMBER)

				' 1. listen
				listener.Start()

				Console.WriteLine("Server started on " & Convert.ToString(listener.LocalEndpoint))
				Console.WriteLine("Waiting for a connection...")

				Dim socket As Socket = listener.AcceptSocket()
				Console.WriteLine("Connection received from " & Convert.ToString(socket.RemoteEndPoint))

				' 2. receive
				Dim data As Byte() = New Byte(BUFFER_SIZE - 1) {}
				socket.Receive(data)

				Dim str As String = encoding.GetString(data)

				' 3. send
				socket.Send(encoding.GetBytes("Hello " & str))

				' 4. close
				socket.Close()

				listener.[Stop]()
			Catch ex As Exception
				Console.WriteLine("Error: " & Convert.ToString(ex))
			End Try
			Console.Read()
		End Sub
	End Class
End Namespace
