Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Diagnostics

Namespace includes
	Class CodemeSystems

		'CodemeSystems.runCommand(@"mkdir testst");

		Public Shared Sub runCommand(Optional anyCommand As String = "")
            Dim proc1 As ProcessStartInfo = New ProcessStartInfo()

			proc1.UseShellExecute = True

			proc1.WorkingDirectory = "C:\Windows\System32"

			proc1.FileName = "C:\Windows\System32\cmd.exe"
			'proc1.Verb = "runas";
			proc1.Arguments = "/c cd\ && " & anyCommand
			proc1.WindowStyle = ProcessWindowStyle.Hidden
			Process.Start(proc1)
		End Sub
	End Class
End Namespace
