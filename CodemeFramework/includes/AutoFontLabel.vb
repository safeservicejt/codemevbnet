Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Windows.Forms
Imports System.Drawing

Namespace includes
	Class AutoFontLabel
		Inherits Label
		Public Sub New()
			MyBase.New()
			Me.AutoEllipsis = True
		End Sub

		Protected Overrides Sub OnPaddingChanged(e As EventArgs)
			UpdateFontSize()
			MyBase.OnPaddingChanged(e)
		End Sub

		Protected Overrides Sub OnResize(e As EventArgs)
			UpdateFontSize()
			MyBase.OnResize(e)
		End Sub

		Private Sub UpdateFontSize()
			Dim textHeight As Integer = Me.ClientRectangle.Height - Me.Padding.Top - Me.Padding.Bottom

			If textHeight > 0 Then
				Me.Font = New Font(Me.Font.FontFamily, textHeight, GraphicsUnit.Pixel)
			End If
		End Sub
	End Class
End Namespace
