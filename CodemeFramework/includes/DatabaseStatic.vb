Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Data.SqlClient
Imports System.Data
Imports CodemeFramework.includes

'
'            try
'            {
'                DatabaseStatic.connect();
'            }
'            catch(Exception ex)
'            {
'                MessageBox.Show(ex.Message);
'
'                throw;
'            }
'
'            DataTable users = new DataTable();
'
'            users = DatabaseStatic.query("select * from users");
'
'            string username = "";
'
'            foreach (DataRow dr in users.Rows)
'            {
'                username += dr["username"].ToString()+" - ";
'            }
'
'            textBox1.Text = username;
' * -------------------------
'            DataTable t = new DataTable();
'
'            t = DatabaseStatic.query(@"select * from users");
'
'            dataGridView1.DataSource = t;
'
' * 
' *             //DatabaseStatic.connect();
'
'            //try
'            //{
'            //    DataTable table;
'
'            //    table = DatabaseStatic.query("select * from users");
'
'            //    DataGridViewRow row;
'
'            //    foreach (DataRow dr in table.Rows)
'            //    {
'            //        row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
'            //        row.Cells[0].Value = dr["id"].ToString();
'            //        row.Cells[1].Value = dr["username"].ToString();
'            //        row.Cells[2].Value = dr["fullname"].ToString();
'            //        dataGridView1.Rows.Add(row);
'            //    }
'
'            //}
'            //catch(Exception ex)
'            //{
'            //    MessageBox.Show(ex.Message);
'            //}
' * 
' * 
' * 
'                for (int i = 0; i < 3; i++)
'                {
'                    row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
'
'                    dr = table.Rows[i];
'
'                    row.Cells[0].Value = dr["id"].ToString();
'                    row.Cells[1].Value = dr["firstname"].ToString();
'                    row.Cells[2].Value = dr["age"].ToString();
'                    row.Cells[3].Value = Image.FromFile(@"D:\jMatter\Learn\C#\Framework\Codeme\CodemeFramework\CodemeFramework\images\1495125586_edit.png");
'
'                    dataGridView1.Rows.Add(row);
'                }
'



Namespace includes
    Class DatabaseStatic

        'Data Source=127.2.3.4\SQLEXPRESS,1433;Network Library=DBMSSOCN;Initial Catalog=dbase;User ID=sa;Password=password

        'Data Source=tcp:127.2.3.4;Initial Catalog=dbase;User ID=sa;Password=password

        'Data Source=MINHTIEN-PC\SQLEXPRESS;Initial Catalog=note_management;Integrated Security=True;Pooling=False

        'Server=myServerName\myInstanceName;Database=myDataBase;User Id=myUsername;Password=myPassword;

        'Data Source=myServerAddress;Initial Catalog=myDataBase;Integrated Security=SSPI;User ID=myDomain\myUsername;Password=myPassword;

        'Server=.\SQLExpress;AttachDbFilename=C:\MyFolder\MyDataFile.mdf;Database=dbname;Trusted_Connection=Yes;

        'Data Source=.\SQLExpress;Integrated Security=true;AttachDbFilename=C:\MyFolder\MyDataFile.mdf;User Instance=true;

        Public Shared cnn As SqlConnection

        Public Shared connectionString As String = Nothing

        Public Shared Sub connect(Optional otherConnection As String = "")
            'SqlConnection cnn ;
            'If otherConnection.Length < 10 Then
            '	'SqlConnection cs = new SqlConnection(@"Data Source=(IP Address)\SQLEXPRESS,1433;Network Library=DBMSSOCN;Initial Catalog=dbase;User ID=sa;Password=password");
            '             connectionString = "Data Source=MINHTIEN-PC\SQLEXPRESS;Initial Catalog=note_management;Integrated Security=True;Pooling=False"
            'Else
            '	connectionString = otherConnection
            'End If

            If otherConnection.Length > 10 Then
                connectionString = otherConnection
            End If

            cnn = New SqlConnection(connectionString)

            Try
                cnn.Open()
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Sub

        Public Shared Function getAdapter(inputQuery As String, Optional inputConnectString As String = "") As SqlDataAdapter
            If inputConnectString = "" Then
                inputConnectString = connectionString
            End If

            Dim adapter As New SqlDataAdapter(inputQuery, inputConnectString)

            Return adapter
        End Function

        Public Shared Sub close()
            cnn.Close()
        End Sub

        Public Shared Sub nonquery(sql As String)
            Dim command As New SqlCommand(sql, cnn)

            Try
                'command.Connection.Open()
                command.ExecuteNonQuery()
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Sub

        Public Shared Function query(sql As String) As DataTable
            '
            '            foreach ( DataRow dr in table.Rows )
            '            {
            '                string name = dr["columnname"].ToString();
            '            }
            '
            '            -----------------------
            '            Database db = new Database();
            '
            '            try
            '            {
            '                db.connect();
            '            }
            '            catch(Exception ex)
            '            {
            '                MessageBox.Show(ex.Message);
            '
            '                throw;
            '            }
            '
            '            DataTable users = new DataTable();
            '
            '            users = db.query("select * from users");
            '
            '            string username = "";
            '
            '            foreach (DataRow dr in users.Rows)
            '            {
            '                username += dr["username"].ToString()+" - ";
            '            }
            '
            '            textBox1.Text = username;            
            '            


            Dim table As New DataTable()

            Try
                Dim adapter As SqlDataAdapter = New SqlDataAdapter()
                adapter.SelectCommand = New SqlCommand(sql, cnn)
                adapter.Fill(table)
            Catch e As Exception
                Throw New Exception(e.Message)
            End Try


            Return table
        End Function

        Public Shared Sub insert(inputTable As String, inputFields As String, inputValues As String)

            'Dim result As String = ""

            Try
                Dim insertData As DataTable = New DataTable()

                insertData = DatabaseStatic.query("insert into " + inputTable + "(" + inputFields + ") values(" + inputValues + ")")

                'result = insertData.Rows(0)("id").ToString()

            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

            'Return result
        End Sub

        Public Shared Function insertWithLastId(inputTable As String, inputFields As String, inputValues As String) As String

            'Dim id As String = DatabaseStatic.insertWithLastId("listgroups", "title", "('sdsd')")

            Dim result As String = ""

            Try
                Dim insertData As DataTable = New DataTable()

                insertData = DatabaseStatic.query("insert into " + inputTable + "(" + inputFields + ") OUTPUT INSERTED.id values(" + inputValues + ")")

                result = insertData.Rows(0)("id").ToString()

            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

            Return result
        End Function

        Public Shared Sub remove()

        End Sub

        Public Shared Sub update(inputList As Dictionary(Of String, String), Optional whereQuery As String = "")

        End Sub

    End Class
End Namespace
