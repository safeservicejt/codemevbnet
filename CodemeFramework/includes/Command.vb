Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.Diagnostics

Namespace includes

    Class Command

        Public Shared Sub restartPC(Optional ByVal delaySeconds As String = "")
            If (delaySeconds <> "") Then
                delaySeconds = (" /t " + delaySeconds)
            End If

            Command.ExecuteCommand(("shutdown /r" + delaySeconds))
        End Sub

        Public Shared Sub shutdownPC(Optional ByVal delaySeconds As String = "")
            If (delaySeconds <> "") Then
                delaySeconds = (" /t " + delaySeconds)
            End If

            Command.ExecuteCommand(("shutdown /s" + delaySeconds))
        End Sub

        Public Shared Sub logoffPC(Optional ByVal delaySeconds As String = "")
            If (delaySeconds <> "") Then
                delaySeconds = (" /t " + delaySeconds)
            End If

            Command.ExecuteCommand(("shutdown /l" + delaySeconds))
        End Sub

        Public Shared Sub ExecuteCommand(ByVal command As String)
            Dim processInfo As ProcessStartInfo = New ProcessStartInfo("cmd.exe", ("/C " + command))
            Dim process As Process = process.Start(processInfo)
            process.WaitForExit()
            'var exitCode = process.ExitCode;
            process.Close()
            'return exitCode;
        End Sub

        Public Shared Function getCommandOutput(inputCommand As String) As String
            Dim proc As New Process()
            Dim oStartInfo As New ProcessStartInfo("cmd.exe", "/c " + inputCommand)
            oStartInfo.UseShellExecute = False
            oStartInfo.RedirectStandardOutput = True
            oStartInfo.CreateNoWindow = True
            proc.StartInfo = oStartInfo
            proc.Start()

            Dim sOutput As String

            Using oStreamReader As System.IO.StreamReader = proc.StandardOutput
                sOutput = oStreamReader.ReadToEnd()
            End Using

            proc.Dispose()

            Return sOutput
        End Function
    End Class
End Namespace