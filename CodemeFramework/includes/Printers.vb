Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Printing

Namespace includes
	Class Printers
		Public Shared Sub fromFile(filePath As String, Optional printerName As String = "")
			'File.Copy("myFileToPrint.pdf", "\\myPrintServerName\myPrinterName")
			'---------------------------
			'LocalPrintServer localPrintServer = new LocalPrintServer();
			'PrintQueue defaultPrintQueue = LocalPrintServer.GetDefaultPrintQueue();

			'''/ Call AddJob
			'PrintSystemJobInfo myPrintJob = defaultPrintQueue.AddJob();

			'''/ Write a Byte buffer to the JobStream and close the stream
			'Stream myStream = myPrintJob.JobStream;
			'Byte[] myByteBuffer = UnicodeEncoding.Unicode.GetBytes("This is a test string for the print job stream.");
			'myStream.Write(myByteBuffer, 0, myByteBuffer.Length);
			'myStream.Close();

			If printerName = "" Then
				printerName = getDefaultPrinter()
			End If

			Try
				System.IO.File.Copy(filePath, printerName)
			Catch ex As Exception
				Throw New Exception(ex.Message)
			End Try


		End Sub

		Public Shared Sub fromText(inputData As String)
			Dim p As New PrintDocument()
			AddHandler p.PrintPage, Sub(sender1 As Object, e1 As PrintPageEventArgs) e1.Graphics.DrawString(inputData, New Font("Times New Roman", 12), New SolidBrush(Color.Black), New RectangleF(0, 0, p.DefaultPageSettings.PrintableArea.Width, p.DefaultPageSettings.PrintableArea.Height))


			Try
				p.Print()
			Catch ex As Exception
				Throw New Exception("Exception Occured While Printing", ex)
			End Try
		End Sub

		Public Shared Function getDefaultPrinter() As String
			Dim settings As New PrinterSettings()
			For Each printer As String In PrinterSettings.InstalledPrinters
				settings.PrinterName = printer
				If settings.IsDefaultPrinter Then
					Return printer
				End If
			Next

			Return String.Empty
		End Function

		Public Shared Function getListPrinters() As String()

			'
'            LocalPrintServer printServer = new LocalPrintServer();
'
'            PrintQueueCollection printQueuesOnLocalServer = printServer.GetPrintQueues(new[] {EnumeratedPrintQueueTypes.Local , EnumeratedPrintQueueTypes.Connections});
'
'            foreach (PrintQueue printer in printQueuesOnLocalServer)
'
'            {
'
'            Debug.WriteLine("\tThe shared printer : " + printer.Name);
'
'            }
'             * 


			Dim result As String() = {}

			Dim i As Integer = 0

			For Each printer As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
				result(i) = printer
				i += 1
			Next

			Return result
		End Function
	End Class
End Namespace
