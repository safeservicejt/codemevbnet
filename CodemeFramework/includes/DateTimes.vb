Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading

Namespace includes
    Class DateTimes

        Public Shared Function fromString(inputString As String) As DateTime
            'Dim iString As String = "2005-05-05 22:12 PM"
            Dim oDate As DateTime = DateTime.ParseExact(inputString, "yyyy-MM-dd HH:mm tt", Nothing)

            Return oDate
        End Function

        Public Shared Function UnixTimeNow() As Long
            Dim timeSpan As TimeSpan = (DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0))
            Return CLng(timeSpan.TotalSeconds)

        End Function

        Public Shared Function daysInMonth(month As Integer, year As Integer) As Integer
            Dim result As Integer = System.DateTime.DaysInMonth(year, month)

            Return result
        End Function

        Public Shared Function dayOfWeek(day As Integer, month As Integer, year As Integer) As String
            Dim dt As New DateTime(year, month, day)

            Dim result As String = dt.DayOfWeek.ToString()

            Return result
        End Function
    End Class
End Namespace
