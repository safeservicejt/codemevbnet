Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Runtime.Serialization
Imports System.Xml
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Cryptography

Namespace includes
    Class Strings

        Public Shared Function isNumberic(strNumber As String) As Boolean
            Dim result As Boolean = False

            If Regex.IsMatch(strNumber, "^[0-9 ]+$") Then
                result = True
            End If

            Return result
        End Function

        Public Shared Function sqlDate() As String
            Dim regDate As DateTime = DateTime.Now
            Dim strDate As String = regDate.ToString("yyyy-MM-dd")

            Return strDate
        End Function

        Public Shared Function sqlTime() As String
            Dim regDate As DateTime = DateTime.Now
            Dim strDate As String = regDate.ToString("HH:mm:ss")

            Return strDate
        End Function

        Public Shared Function sqlDateTime() As String
            Dim regDate As DateTime = DateTime.Now
            Dim strDate As String = regDate.ToString("yyyy-MM-dd HH:mm:ss")

            Return strDate
        End Function

        Public Shared Function convertToUnSign3(s As String) As String
            Dim regex As New Regex("\p{IsCombiningDiacriticalMarks}+")
            Dim temp As String = s.Normalize(NormalizationForm.FormD)
            Return regex.Replace(temp, [String].Empty).Replace("đ"c, "d"c).Replace("Đ"c, "D"c)
        End Function

        Public Shared Function addslash(Optional inputData As String = "") As String
            inputData = System.Text.RegularExpressions.Regex.Replace(inputData, "[\042\047]", "\$0")

            Return inputData
        End Function

        Public Shared Function stripslash(Optional inputData As String = "") As String
            inputData = System.Text.RegularExpressions.Regex.Replace(inputData, "(\\)([\042\047])", "$2")

            Return inputData
        End Function

        Public Shared Function randNumber(Optional len As Integer = 10) As String
            Dim result As String = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"

            result = randString(result, len)

            Return result
        End Function

        Public Shared Function randAll(Optional len As Integer = 10) As String
            Dim result As String = "!@#$%^&*()_+-=[]{}:'""|<>?,./!@#$%^&*()_+-=[]{}:'""|<>?,./qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789"

            result = randString(result, len)

            Return result
        End Function

        Public Shared Function implode(Optional inputChar As String = ",", Optional inputData As String() = Nothing) As String
            Dim result As String = ""

            result = [String].Join(inputChar, inputData)

            Return result
        End Function

        Public Shared Function splitLines(Optional inputData As String = "") As String()
            Dim result As String() = {}

            result = inputData.Split(New String() {vbCr & vbLf, vbLf}, StringSplitOptions.None)

            Return result
        End Function

        Public Shared Function explode(Optional inputChar As Char = ","c, Optional inputData As String = Nothing) As String()
            Dim result As String() = {}

            If TypeOf inputData Is [String] Then
                result = inputData.Split(inputChar)
            End If

            Return result
        End Function

        'outputStr.Text = Strings.preg_replace(inputData, new string[] { pattern }, new string[] { template });
        Public Shared Function preg_replace(inputData As String, pattern As String(), replacements As String(), Optional allowMultiLines As Boolean = False) As String
            'var result = Regex.Replace("david", "[b-df-hj-np-tv-z]", "$0o$0" );

            'String[] pattern = new String[4];
            'String[] replacement = new String[4];

            'pattern[0] = "Quick";
            'pattern[1] = "Fox";
            'pattern[2] = "Jumped";
            'pattern[3] = "Lazy";

            'replacement[0] = "Slow";
            'replacement[1] = "Turtle";
            'replacement[2] = "Crawled";
            'replacement[3] = "Dead";

            'String DemoText = "The Quick Brown Fox Jumped Over the Lazy Dog";

            'Console.WriteLine(DemoText.PregReplace(pattern, replacement));


            If replacements.Length <> pattern.Length Then
            End If

            For i As Integer = 0 To pattern.Length - 1
                If allowMultiLines Then
                    inputData = Regex.Replace(inputData, pattern(i), replacements(i), RegexOptions.Multiline Or RegexOptions.IgnoreCase)
                Else
                    inputData = Regex.Replace(inputData, pattern(i), replacements(i), RegexOptions.IgnoreCase)

                End If
            Next

            Return inputData
        End Function

        Public Shared Function preg_match(Optional pattern As String = "", Optional inputData As String = "") As String()
            Dim result(20) As String

            Dim regex As New Regex(pattern)
            Dim match As Match = regex.Match(inputData)


            If match.Success Then

                Dim totalGroup As Integer = match.Groups.Count - 1


                For i As Integer = 1 To totalGroup
                    result(i) = match.Groups(i).Value

                Next
            End If

            Return result
        End Function

        Public Shared Function md5(s As String) As String
            Using provider As MD5 = System.Security.Cryptography.MD5.Create()
                Dim builder As New StringBuilder()

                For Each b As Byte In provider.ComputeHash(Encoding.UTF8.GetBytes(s))
                    builder.Append(b.ToString("x2").ToLower())
                Next

                Return builder.ToString()
            End Using
        End Function


        Public Shared Function base64_encode(plainText As String) As String
            Dim plainTextBytes As Byte() = Encoding.UTF8.GetBytes(plainText)
            Return System.Convert.ToBase64String(plainTextBytes)
        End Function

        Public Shared Function base64_decode(plainText As String) As String
            Dim base64EncodedBytes As Byte() = System.Convert.FromBase64String(plainText)
            Return System.Text.Encoding.UTF8.GetString(base64EncodedBytes)
        End Function



        Public Shared Function preg_match_all(Optional pattern As String = "", Optional inputData As String = "", Optional totalRow As Integer = 9) As Dictionary(Of Integer, Dictionary(Of Integer, String))
            Dim listResult As New Dictionary(Of Integer, Dictionary(Of Integer, String))()

            For i As Integer = 1 To totalRow
                listResult(i) = New Dictionary(Of Integer, String)()
            Next

            'listResult[1] = new Dictionary<int, string>();
            'listResult[2] = new Dictionary<int, string>();
            'listResult[3] = new Dictionary<int, string>();
            'listResult[4] = new Dictionary<int, string>();
            'listResult[5] = new Dictionary<int, string>();
            'listResult[6] = new Dictionary<int, string>();
            'listResult[7] = new Dictionary<int, string>();
            'listResult[8] = new Dictionary<int, string>();
            'listResult[9] = new Dictionary<int, string>();

            Dim ctr As Integer = 0

            For Each match As Match In Regex.Matches(inputData, pattern, RegexOptions.IgnoreCase)
                For i As Integer = 1 To match.Groups.Count - 1
                    ctr = listResult(i).Count

                    listResult(i)(ctr) = match.Groups(i).Value.ToString()
                Next
            Next

            Return listResult
        End Function


        Public Shared Function randAlphabet(Optional len As Integer = 10) As String
            Dim result As String = "qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm"

            result = randString(result, len)

            Return result
        End Function

        Public Shared Function randText(Optional len As Integer = 10) As String
            Dim result As String = "qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789qwertyuiopasdfghjklzxcvbnm0123456789"

            result = randString(result, len)

            Return result
        End Function

        Public Shared Function randString(inputData As String, Optional len As Integer = 10) As String
            If inputData.Length > 0 Then
                inputData = shuffle(inputData)

                inputData = inputData.Substring(0, len)
            End If

            Return inputData
        End Function

        Public Shared Function shuffle(inputData As String) As String
            Dim num As New Random()

            ' Create new string from the reordered char array
            Dim rand As New String(inputData.ToCharArray().OrderBy(Function(s) (num.[Next](2) Mod 2) = 0).ToArray())

            Return rand
        End Function


    End Class
End Namespace
