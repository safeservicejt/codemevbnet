Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Collections
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

'
'studentArray.Where(s => s.Age > 12 && s.Age < 20)
'            .ToList()
'            .ForEach(s => Console.WriteLine(item.StudentName));
' * 
'List<Student> teenAgerStudent = studentArray.Where(s => s.Age > 12 && s.Age < 20).ToList();
'teenAgerStudent.ForEach(s => Console.WriteLine(s.StudentName));
' * 
'  // There's no need in materialization, i.e. "ToArray()"
'  var target = studentArray 
'    .Where(student => student.Age > 12 && student.Age < 20) // teen
'    .Select(student => String.Format("Id: {0} Name: {1} Age {2}", 
'              student.Id, student.Name, student.Age));
'
'  // Printing out in one line (thanks to String.Join)
'  Console.Write(String.Join(Environment.NewLine, target));
' * 
'//get the teenager students in form of IEnumerable<Student>.
'//cast it to array with .ToArray() after .Where() if needed.
'var teenAgerStudents = studentArray.Where(s => s.Age > 12 && s.Age < 20);
'//print only the students' names
'Console.Write(string.Join(Environment.NewLine,teenAgerStudents.Select(s=>s.StudentName)));
' * 
' * 
'    Array.Sort(intArray);
' * 
' * 
'// array of custom type
'User[] users = new User[3] { new User("Betty", 23),  // name, age
'                             new User("Susan", 20),
'                             new User("Lisa", 25) };
' * 
' * 
' // sort array by name
'Array.Sort(users, delegate(User user1, User user2) {
'                    return user1.Name.CompareTo(user2.Name);
'                  });
'// write array (output: Betty23 Lisa25 Susan20)
'
' * 
'Array.Sort(users, delegate(User user1, User user2) {
'                    return user1.Age.CompareTo(user2.Age); // (user1.Age - user2.Age)
'                  });
'// write array (output: Susan20 Betty23 Lisa25)
' * 
' * 
' * 
' 

Namespace includes
    Class Arrays

        Private listString As New List(Of String)

        Public Sub add(inputStr As String)
            listString.Add(inputStr)
        End Sub

        Public Function getList() As List(Of String)
            Return listString
        End Function


        Public Shared Function array_unique([handles] As String()) As String()
            Return [handles].ToList().Distinct().ToArray()
        End Function

        Public Shared Function array_keys(inputData As IDictionary) As String()
            '
            '            IDictionary<string,string> openWith = new Dictionary<string,string>()
            '            {
            '                { "txt", "notepad.exe" }
            '                { "bmp", "paint.exe" }
            '                { "rtf", "wordpad.exe" }
            '            };
            '
            '            data.Add("abc", 123);
            '            


            Dim result As String() = {}

            Dim i As Integer = 0

            For Each item As DictionaryEntry In inputData
                result(i) = item.Key.ToString()

                i += 1
            Next

            Return result

        End Function

        Public Shared Function array_values(inputData As IDictionary) As String()
            '
            '            IDictionary<string,string> openWith = new Dictionary<string,string>()
            '            {
            '                { "txt", "notepad.exe" }
            '                { "bmp", "paint.exe" }
            '                { "rtf", "wordpad.exe" }
            '            };
            '
            '            data.Add("abc", 123);
            '            


            Dim result As String() = {}

            Dim i As Integer = 0

            For Each item As DictionaryEntry In inputData
                result(i) = item.Value.ToString()

                i += 1
            Next

            Return result

        End Function

        Public Shared Function jObjectToJson(inputData As JObject) As String
            '
            '            var obj = new JObject();
            '
            '            obj["One"] = "Value One";
            '            obj["Two"] = "Value Two";
            '            obj["Three"] = "Value Three";
            '             


            Dim serialized As String = JsonConvert.SerializeObject(inputData)

            Return serialized
        End Function

        Public Shared Function serializeObject(inputData As JObject) As String
            '
            '            var obj = new JObject();
            '
            '            obj["One"] = "Value One";
            '            obj["Two"] = "Value Two";
            '            obj["Three"] = "Value Three";
            '             


            '
            '            var o = JObject.Parse(stringFullOfJson);
            '            var page = (int)o["page"];
            '            var totalPages = (int)o["total_pages"];
            '             


            Dim serialized As String = JsonConvert.SerializeObject(inputData)

            Return serialized
        End Function

        'Dictionary<int, Dictionary<int, string>> listOutput = Arrays.make();
        Public Shared Function make(Optional totalRow As Integer = 9) As Dictionary(Of Integer, Dictionary(Of Integer, String))
            Dim listResult As New Dictionary(Of Integer, Dictionary(Of Integer, String))()

            For i As Integer = 1 To totalRow
                listResult(i) = New Dictionary(Of Integer, String)()
            Next

            Return listResult
        End Function

    End Class
End Namespace
