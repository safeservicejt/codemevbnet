﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Windows.Forms
Imports System.Threading
Imports System.IO.Ports


Public Class ModernSerialPorts

    Public Shared canContinue As Boolean = False

    Public Shared theSerialPort As SerialPort

    Public Shared inputData As String = ""

    Public Shared outputData As String = ""

    Public Shared readThread As Thread

    Private Shared buffer As String = ""

    Public Shared Function listPorts() As String()
        Dim result As String() = System.IO.Ports.SerialPort.GetPortNames

        Return result
    End Function

    Public Sub connectTo(inputPortName As String, Optional inputBaudrate As Integer = 9600)

        With theSerialPort

            .ParityReplace = &H3B                    ' replace ";" when parity error occurs 
            .PortName = inputPortName
            .BaudRate = inputBaudrate
            .Parity = IO.Ports.Parity.None
            .DataBits = 8
            .StopBits = IO.Ports.StopBits.One
            .Handshake = IO.Ports.Handshake.None
            .RtsEnable = False
            .ReceivedBytesThreshold = 1             'threshold: one byte in buffer > event is fired 
            .NewLine = vbCr         ' CR must be the last char in frame. This terminates the SerialPort.readLine 
            .ReadTimeout = 10000
            AddHandler .DataReceived, AddressOf DataReceivedHandler
        End With

        canContinue = True

        If theSerialPort.IsOpen Then
            Throw New Exception("Port: " + inputPortName + " is opened.")
        End If

        Try
            theSerialPort.Open()


        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If theSerialPort Is Nothing Then
                theSerialPort.Close()
            End If
        End Try


    End Sub

    Private Sub DataReceivedHandler(ByVal sender As System.Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs)
        Try
            'Dim rcv As String = theSerialPort.ReadExisting()
            'buffer = String.Concat(buffer, rcv)

            'Dim x As Integer
            'Do
            '    x = buffer.IndexOf(vbCrLf)
            '    If x > -1 Then
            '        Console.WriteLine(buffer.Substring(0, x).Trim())
            '        buffer = buffer.Remove(0, x + 2)
            '    End If
            'Loop Until x = -1
            If canContinue And theSerialPort.IsOpen() Then
                buffer = theSerialPort.ReadExisting()

                parseInputData(buffer)
            End If


        Catch ex As Exception

        End Try

    End Sub

    Private Sub parseInputData(inputStr As String)

    End Sub

    Public Sub closePort()
        theSerialPort.DiscardInBuffer()
        theSerialPort.Close()
    End Sub

End Class
