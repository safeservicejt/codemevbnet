Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Net.Sockets

Namespace includes
	Class SocketClientsStream
		Private Const BUFFER_SIZE As Integer = 1024
		Private Const PORT_NUMBER As Integer = 9999

		Shared encoding As New ASCIIEncoding()

		Public Shared Sub Start()

			Try
				Dim client As New TcpClient()

				' 1. connect
				client.Connect("127.0.0.1", PORT_NUMBER)
				Dim stream As Stream = client.GetStream()

				Console.WriteLine("Connected to Y2Server.")
				While True
					Console.Write("Enter your name: ")

					Dim str As String = Console.ReadLine()
                    Dim reader As StreamReader = New StreamReader(stream)
                    Dim writer As StreamWriter = New StreamWriter(stream)
					writer.AutoFlush = True

					' 2. send
					writer.WriteLine(str)

					' 3. receive
					str = reader.ReadLine()
					Console.WriteLine(str)
					If str.ToUpper() = "BYE" Then
						Exit While
					End If
				End While
				' 4. close
				stream.Close()
				client.Close()

			Catch ex As Exception
				Console.WriteLine("Error: " & Convert.ToString(ex))
			End Try

			Console.Read()
		End Sub
	End Class
End Namespace
