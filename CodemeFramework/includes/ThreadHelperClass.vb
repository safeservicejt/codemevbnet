﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Windows.Forms
Imports System.Diagnostics
Imports System.IO
Imports System.Drawing
Imports CodemeFramework.includes

Public Class ThreadHelperClass


    ' this.Invoke(new MethodInvoker(delegate() { txtLogs.Text += "Received: " + inputData + "\r\n"; }));


    'Private Sub btnTestThread_Click(sender As Object, e As EventArgs)
    '    Dim demoThread As New Thread(New ThreadStart(AddressOf Me.ThreadProcSafe))
    '    demoThread.Start()
    'End Sub

    '' This method is executed on the worker thread and makes 
    '' a thread-safe call on the TextBox control. 
    'Private Sub ThreadProcSafe()
    '    ThreadHelperClass.SetText(Me, textBox1, "This text was set safely.")
    '    ThreadHelperClass.SetText(Me, textBox2, "another text was set safely.")
    'End Sub


    Private Sub New()
    End Sub
    Private Delegate Sub SetTextCallback(f As Form, ctrl As Control, text As String)
    ''' <summary>
    ''' Set text property of various controls
    ''' </summary>
    ''' <param name="form">The calling form</param>
    ''' <param name="ctrl"></param>
    ''' <param name="text"></param>
    Public Shared Sub SetText(form As Form, ctrl As Control, text As String)
        ' InvokeRequired required compares the thread ID of the 
        ' calling thread to the thread ID of the creating thread. 
        ' If these threads are different, it returns true. 
        If ctrl.InvokeRequired Then
            Try
                Dim d As New SetTextCallback(AddressOf SetText)
                form.Invoke(d, New Object() {form, ctrl, text})
            Catch ex As Exception

            End Try

        Else
            ctrl.Text = text
        End If
    End Sub
End Class
