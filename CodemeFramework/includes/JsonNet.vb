Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Threading
Imports System.Xml.Serialization
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Serialization
Imports CodemeFramework.includes

Namespace includes
    Class JsonNet

        Public Shared Function encodeDataSet(dataSet As DataSet) As String
            'DataSet dataSet = new DataSet("dataSet");
            'dataSet.Namespace = "NetFrameWork";
            'DataTable table = new DataTable();
            'DataColumn idColumn = new DataColumn("id", typeof(int));
            'idColumn.AutoIncrement = true;

            'DataColumn itemColumn = new DataColumn("item");
            'table.Columns.Add(idColumn);
            'table.Columns.Add(itemColumn);
            'dataSet.Tables.Add(table);

            'for (int i = 0; i < 2; i++)
            '{
            '    DataRow newRow = table.NewRow();
            '    newRow["item"] = "item " + i;
            '    table.Rows.Add(newRow);
            '}

            'dataSet.AcceptChanges();

            Dim json As String = JsonConvert.SerializeObject(dataSet, Formatting.Indented)
            Return json
        End Function

        Public Shared Function encodeDataTable(dataTable As DataTable) As String
            Dim json As String = JsonConvert.SerializeObject(dataTable, Formatting.Indented)
            Return json
        End Function

        Public Shared Function encodeDictionary(inputData As Dictionary(Of String, String)) As String
            Dim json As String = JsonConvert.SerializeObject(inputData, Formatting.Indented)

            Return json
        End Function

        Public Shared Function decodeDataTable(inputData As String) As DataTable
            Dim dataTable As DataTable = JsonConvert.DeserializeObject(Of DataTable)(inputData)

            Return dataTable
        End Function

        Public Shared Function decodeDataSet(inputData As String) As DataSet
            '            string json = @"{
            '              'Table1': [
            '                {
            '                  'id': 0,
            '                  'item': 'item 0'
            '                },
            '                {
            '                  'id': 1,
            '                  'item': 'item 1'
            '                }
            '              ]
            '            }";

            '            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json);

            '            DataTable dataTable = dataSet.Tables["Table1"];

            '            Console.WriteLine(dataTable.Rows.Count);
            '            // 2

            '            foreach (DataRow row in dataTable.Rows)
            '            {
            '                Console.WriteLine(row["id"] + " - " + row["item"]);
            '            }
            '            // 0 - item 0
            '            // 1 - item 1

            Dim dataSet As DataSet = JsonConvert.DeserializeObject(Of DataSet)(inputData)

            Return dataSet
        End Function

        Public Shared Function decodeDictionary(inputData As String) As Dictionary(Of String, String)
            Dim outData As Dictionary(Of String, String) = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(inputData)

            Return outData
        End Function

        'Public Shared Function decodeMultiDictionary(inputData As String) As MultiKeyDictionary(Of String, String, String)
        '    Dim outData As MultiKeyDictionary(Of String, String, String) = JsonConvert.DeserializeObject(Of MultiKeyDictionary(Of String, String, String))(inputData)

        '    Return outData
        'End Function

    End Class
End Namespace
