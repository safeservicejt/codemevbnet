Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Threading
Imports CodemeFramework.includes

Namespace includes

    Class Cache

        Public Shared Function getFromIni(ByVal fileName As String) As Dictionary(Of String, String)

            Dim dict As New Dictionary(Of String, String)()

            Try
                dict = Dictionaries.getFromIni(fileName)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

            Return dict
        End Function

        Public Shared Sub make(Optional ByVal keyName As String = "", Optional ByVal fileData As String = "")
            Dim cachePath As String = (App.rootPath + "\caches\")
            Dim savePath As String = (cachePath + (keyName + ".cache"))
            Dim cacheTimePath As String = (cachePath + "\times\")
            Dim saveTime As String = DateTimes.UnixTimeNow.ToString
            Dim saveTimePath As String = (cacheTimePath  + (keyName + "_time.cache"))
            Files.make(savePath, fileData)
            Files.make(saveTimePath, saveTime)
        End Sub

        Public Shared Sub delete(ByVal keyName As String)
            Dim cachePath As String = (App.rootPath + "\caches\")
            Dim savePath As String = (cachePath + (keyName + ".cache"))
            Dim cacheTimePath As String = (cachePath + "\times\")
            Dim saveTime As String = DateTimes.UnixTimeNow.ToString
            Dim saveTimePath As String = (cacheTimePath + (keyName + "_time.cache"))
            Files.delete(savePath)
            Files.delete(saveTimePath)
        End Sub

        Public Shared Function getData(Optional ByVal keyName As String = "", Optional ByVal live As Integer = 0) As String
            Dim result As String = ""
            Dim cachePath As String = (App.rootPath + "\caches\")
            Dim savePath As String = (cachePath + (keyName + ".cache"))
            Dim cacheTimePath As String = (cachePath + "\times\")
            Dim saveTime As String = DateTimes.UnixTimeNow.ToString
            Dim saveTimePath As String = (cacheTimePath + (keyName + "_time.cache"))
            Dim timeTmp As String = System.IO.File.ReadAllText(saveTimePath)
            Dim timeLiveSaved As Long = Int32.Parse(timeTmp)
            Dim curTimeLive As Long = DateTimes.UnixTimeNow
            If (live > 0) Then
                Dim TTL As Long = (curTimeLive - timeLiveSaved)
                If (TTL <= live) Then
                    If System.IO.File.Exists(savePath) Then
                        result = System.IO.File.ReadAllText(savePath)
                    End If

                End If

            ElseIf (live = 0) Then
                If System.IO.File.Exists(savePath) Then
                    result = System.IO.File.ReadAllText(savePath)
                End If

            End If

            Return result
        End Function
    End Class
End Namespace