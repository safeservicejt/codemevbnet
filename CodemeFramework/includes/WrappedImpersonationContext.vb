Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Runtime.InteropServices
Imports System.Security.Permissions
Imports System.Security.Principal
Imports System.ComponentModel
Imports System.Diagnostics

Namespace includes
	'var impersonationContext = new WrappedImpersonationContext(Domain, Username, Password);
	'impersonationContext.Enter();

	'''/do your stuff here

	'impersonationContext.Leave();

	Public NotInheritable Class WrappedImpersonationContext
		Public Enum LogonType As Integer
			Interactive = 2
			Network = 3
			Batch = 4
			Service = 5
			Unlock = 7
			NetworkClearText = 8
			NewCredentials = 9
		End Enum

		Public Enum LogonProvider As Integer
			[Default] = 0
			' LOGON32_PROVIDER_DEFAULT
			WinNT35 = 1
			WinNT40 = 2
			' Use the NTLM logon provider.
			WinNT50 = 3
			' Use the negotiate logon provider.
		End Enum

		<DllImport("advapi32.dll", EntryPoint := "LogonUserW", SetLastError := True, CharSet := CharSet.Unicode)> _
		Public Shared Function LogonUser(lpszUsername As [String], lpszDomain As [String], lpszPassword As [String], dwLogonType As LogonType, dwLogonProvider As LogonProvider, ByRef phToken As IntPtr) As Boolean
		End Function

		<DllImport("kernel32.dll")> _
		Public Shared Function CloseHandle(handle As IntPtr) As Boolean
		End Function

		Private _domain As String, _password As String, _username As String
		Private _token As IntPtr
		Private _context As WindowsImpersonationContext

		Private ReadOnly Property IsInContext() As Boolean
			Get
				Return _context IsNot Nothing
			End Get
		End Property

		Public Sub New(domain As String, username As String, password As String)
			_domain = If([String].IsNullOrEmpty(domain), ".", domain)
			_username = username
			_password = password
		End Sub

		' Changes the Windows identity of this thread. Make sure to always call Leave() at the end.
		<PermissionSetAttribute(SecurityAction.Demand, Name := "FullTrust")> _
		Public Sub Enter()
			If IsInContext Then
				Return
			End If

			_token = IntPtr.Zero
			Dim logonSuccessfull As Boolean = LogonUser(_username, _domain, _password, LogonType.NewCredentials, LogonProvider.WinNT50, _token)
			If Not logonSuccessfull Then
				Throw New Win32Exception(Marshal.GetLastWin32Error())
			End If
			Dim identity As New WindowsIdentity(_token)
			_context = identity.Impersonate()

			Debug.WriteLine(WindowsIdentity.GetCurrent().Name)
		End Sub

		<PermissionSetAttribute(SecurityAction.Demand, Name := "FullTrust")> _
		Public Sub Leave()
			If Not IsInContext Then
				Return
			End If

			_context.Undo()

			If _token <> IntPtr.Zero Then
				CloseHandle(_token)
			End If
			_context = Nothing
		End Sub

	End Class
End Namespace
