Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Net
Imports System.IO

Namespace includes
	Class Http

        Public Shared Function getData(url As String) As String
            Dim result As String = ""

            Dim client As New WebClient()

            ' Add a user agent header in case the 
            ' requested URI contains a query.

            'client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()

            data.Close()
            reader.Close()

            Return result
        End Function

        Public Shared Function postData(URI As String, Parameters As String, Optional proxyStr As String = "") As String
            Dim req As System.Net.WebRequest = System.Net.WebRequest.Create(URI)

            If proxyStr.Length > 5 Then
                req.Proxy = New System.Net.WebProxy(proxyStr, True)
            End If
            'Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded"
            req.Method = "POST"
            'We need to count how many bytes we're sending. 
            'Post'ed Faked Forms should be name=value&
            Dim bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(Parameters)
            req.ContentLength = bytes.Length
            Dim os As System.IO.Stream = req.GetRequestStream()
            os.Write(bytes, 0, bytes.Length)
            'Push it out there
            os.Close()
            Dim resp As System.Net.WebResponse = req.GetResponse()
            If resp Is Nothing Then
                Return Nothing
            End If
            Dim sr As New System.IO.StreamReader(resp.GetResponseStream())
            Return sr.ReadToEnd().Trim()
        End Function

		'Download file
		'private void BtnDownload_Click(object sender, RoutedEventArgs e)
		'{
		'    using (WebClient wc = new WebClient())
		'    {
		'        wc.DownloadProgressChanged += wc_DownloadProgressChanged;
		'        wc.DownloadFileAsync(new System.Uri("http://www.sayka.in/downloads/front_view.jpg"),
		'        "D:\\Images\\front_view.jpg");
		'    }
		'}

		'void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		'{
		'    progressBar.Value = e.ProgressPercentage;
		'}

		Public Sub downloadFile(fileUrl As String, saveFile As String)

            Using client As WebClient = New WebClient()
                Try
                    client.DownloadFile(fileUrl, saveFile)
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
		End Sub
	End Class
End Namespace
