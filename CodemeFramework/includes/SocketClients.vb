Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Net.Sockets

Namespace includes
	Public Class SocketClients
		Private Const BUFFER_SIZE As Integer = 1024
		Private Const PORT_NUMBER As Integer = 9999

		Shared encoding As New ASCIIEncoding()

		Public Shared Sub Start()

			Try
				Dim client As New TcpClient()

				' 1. connect
				client.Connect("127.0.0.1", PORT_NUMBER)
				Dim stream As Stream = client.GetStream()

				Console.WriteLine("Connected to Codeme Socket Server.")
				Console.Write("Enter your name: ")

				Dim str As String = Console.ReadLine()

				' 2. send
				Dim data As Byte() = encoding.GetBytes(str)

				stream.Write(data, 0, data.Length)

				' 3. receive
				data = New Byte(BUFFER_SIZE - 1) {}
				stream.Read(data, 0, BUFFER_SIZE)

				Console.WriteLine(encoding.GetString(data))

				' 4. Close
				stream.Close()
				client.Close()

			Catch ex As Exception
				Console.WriteLine("Error: " & Convert.ToString(ex))
			End Try

			Console.Read()
		End Sub
	End Class
End Namespace
