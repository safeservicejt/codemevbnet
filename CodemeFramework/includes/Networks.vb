Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Security.Principal
Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Net.NetworkInformation

Imports CodemeFramework.includes

Namespace includes

    'AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
    'WindowsIdentity wid = new WindowsIdentity(username, password);
    'WindowsImpersonationContext context = wid.Impersonate();
    'File.Move(pathToSourceFile, "\\\\Server\\Folder");
    'context.Undo();
    'public static void SaveACopyfileToServer(string filePath, string savePath)
    '    {
    '        var directory = Path.GetDirectoryName(savePath).Trim();
    '        var username = "loginusername";
    '        var password = "loginpassword";
    '        var filenameToSave = Path.GetFileName(savePath);
    '        if (!directory.EndsWith("\\"))
    '            filenameToSave = "\\" + filenameToSave;
    '        var command = "NET USE " + directory + " /delete";
    '        ExecuteCommand(command, 5000);
    '        command = "NET USE " + directory + " /user:" + username + " " + password;
    '        ExecuteCommand(command, 5000);
    '        command = " copy \"" + filePath + "\"  \"" + directory + filenameToSave + "\"";
    '        ExecuteCommand(command, 5000);
    '        command = "NET USE " + directory + " /delete";
    '        ExecuteCommand(command, 5000);
    '    }
    Public Class Networks

        Private Shared tryLogin As Impersonator

        Public Sub loginToComputer(ByVal domain As String, ByVal userName As String, ByVal password As String)
            tryLogin = New Impersonator(userName, domain, password)
        End Sub

        Public Sub Dispose()
            tryLogin.Dispose()
        End Sub

        Public Shared Sub createMapDrive(Optional ByVal driveLetter As String = "", Optional ByVal hostAddress As String = "", Optional ByVal userName As String = "", Optional ByVal passWord As String = "")
            driveLetter = driveLetter.Trim
            Dim cmd As String = ("net use " _
                        + (driveLetter + (": " + hostAddress)))
            If (userName.Length > 1) Then
                cmd = (cmd + (" /user " _
                            + (userName + (" " + passWord))))
            End If

            Command.ExecuteCommand(cmd)
        End Sub

        Public Shared Sub deleteMapDrive(Optional ByVal driveLetter As String = "")
            driveLetter = driveLetter.Trim
            Dim cmd As String = ("net use " + (driveLetter + ": /Delete"))
            Command.ExecuteCommand(cmd)
        End Sub

        Public Shared Sub pingHost(ByVal nameOrAddress As String)

            If (nameOrAddress.Length < 5) Then
                Throw New Exception("Host address: " + nameOrAddress + " not valid.")
            End If

            Dim pinger As Ping = New Ping
            Try
                Dim reply As PingReply = pinger.Send(nameOrAddress)

                If Not reply.Status = IPStatus.Success Then
                    Throw New Exception("Can not ping to: " + nameOrAddress)
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try


        End Sub
    End Class
End Namespace