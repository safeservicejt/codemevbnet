Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Xml
Imports System.Xml.Linq
Imports System.IO

Namespace includes
	Class XML
		'http://www.dotnetcurry.com/linq/564/linq-to-xml-tutorials-examples

        'Dim doc As XmlDocument = New XmlDocument()
        'doc.Load("D:\member.xml")
        'Dim kills As String = doc.SelectSingleNode("Tabel/Member[Naam='Ghostbullet93']/Kills").InnerText

        'Dim members As XmlDocument = doc.SelectNodes("Tabel/Member")

        '------------------------
        'Dim dom As New XmlDocument
        '    dom.Load(Application.StartupPath & "\Settings.xml")
        'Dim objCurrentNode As XmlNode
        '    objCurrentNode = dom.SelectSingleNode("//mimetypes")
        ''now go through all child nodes.
        '    If objCurrentNode.HasChildNodes Then
        ''loop
        'Dim xmlMimeType As XmlNode
        '        For Each xmlMimeType In objCurrentNode
        '            sMimeExt = xmlMimeType.Name
        '            sMimeType = xmlMimeType.InnerText
        '            If (sMimeExt = sFileExt) Then
        '                Exit For
        '            End If
        '        Next
        '    End If
        '-----------------------

		'XElement xelement = XElement.Load("..\\..\\Employees.xml");
		'IEnumerable<XElement> employees = xelement.Elements();
		'Console.WriteLine("List of all Employee Names :");
		'foreach (var employee in employees)
		'{
		'    Console.WriteLine(employee.Element("Name").Value);
		'}

		'XElement xelement = XElement.Load("..\\..\\Employees.xml");
		'IEnumerable<XElement> employees = xelement.Elements();
		'Console.WriteLine("List of all Employee Names along with their ID:");
		'foreach (var employee in employees)
		'{
		'    Console.WriteLine("{0} has Employee ID {1}",
		'        employee.Element("Name").Value,
		'        employee.Element("EmpId").Value);
		'}

		'How Do I access Specific Element having a Specific Attribute using LINQ to XML
		'XElement xelement = XElement.Load("..\\..\\Employees.xml");
		'var name = from nm in xelement.Elements("Employee")
		'           where (string)nm.Element("Sex") == "Female"
		'           select nm;
		'Console.WriteLine("Details of Female Employees:");
		'foreach (XElement xEle in name)
		'    Console.WriteLine(xEle);

		'How Do I Find an Element within another Element using LINQ to XML
		'XElement xelement = XElement.Load("..\\..\\Employees.xml");
		'var addresses = from address in xelement.Elements("Employee")
		'                where (string)address.Element("Address").Element("City") == "Alta"
		'               select address;
		'Console.WriteLine("Details of Employees living in Alta City");
		'foreach (XElement xEle in addresses)
		'    Console.WriteLine(xEle);

		'How Do I Find Nested Elements (using Descendants Axis) using LINQ to XML
		'XElement xelement = XElement.Load("..\\..\\Employees.xml");
		'Console.WriteLine("List of all Zip Codes");
		'foreach (XElement xEle in xelement.Descendants("Zip"))
		'{
		'    Console.WriteLine((string)xEle);
		'}

		'How do I apply Sorting on Elements using LINQ to XML
		'XElement xelement = XElement.Load("..\\..\\Employees.xml");
		'IEnumerable<string> codes = from code in xelement.Elements("Employee")
		'                            let zip = (string)code.Element("Address").Element("Zip")
		'                            orderby zip
		'                            select zip;
		'Console.WriteLine("List and Sort all Zip Codes");

		'foreach (string zp in codes)
		'    Console.WriteLine(zp);


		'Save the XML Document to a XMLWriter or to the disk using LINQ to XML
		'XNamespace empNM = "urn:lst-emp:emp";

		'XDocument xDoc = new XDocument(
		'            new XDeclaration("1.0", "UTF-16", null),
		'            new XElement(empNM + "Employees",
		'                new XElement("Employee",
		'                    new XComment("Only 3 elements for demo purposes"),
		'                    new XElement("EmpId", "5"),
		'                    new XElement("Name", "Kimmy"),
		'                    new XElement("Sex", "Female")
		'                    )));

		'StringWriter sw = new StringWriter();
		'XmlWriter xWrite = XmlWriter.Create(sw);
		'xDoc.Save(xWrite);
		'xWrite.Close();

		'''/ Save to Disk
		'xDoc.Save("C:\\Something.xml");
        Public message As String = ""

        Public Shared fileDoc As XmlDocument = New XmlDocument()

        Public Shared fileList As New Dictionary(Of String, String)()

        Public Shared Sub loadFile(filePath As String, Optional keyName As String = "")

            If (keyName.Length > 0 And fileList.ContainsKey(keyName)) Then
                filePath = fileList(keyName)
            End If

            If (File.Exists(filePath)) Then
                Try
                    Dim fileDoc As XmlDocument = New XmlDocument()
                    fileDoc.Load(filePath)
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try

            End If


        End Sub

        Public Shared Function getInnerText(path As String, Optional keyName As String = "") As String

            If (keyName.Length > 0 And fileList.ContainsKey(keyName)) Then
                Try
                    loadFile(keyName)
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End If

            Dim result As String = fileDoc.SelectSingleNode(path).InnerText

            Return result

        End Function

	End Class
End Namespace
