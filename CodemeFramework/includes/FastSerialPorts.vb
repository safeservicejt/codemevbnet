﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.IO.Ports
Imports System.Threading
Imports System.Diagnostics
Imports System.Net
Imports System.Net.Sockets
Imports System.Net.NetworkInformation
Imports System.Web.Services.Protocols
Imports System.Runtime.InteropServices
Imports Microsoft.Win32
Imports System.Windows.Forms
Imports CodemeFramework.includes


'theSerialPort.ReadTimeout = 3000

'theSerialPort.WriteTimeout = 3000

'theSerialPort.BaudRate = 9600

'theSerialPort.Parity = Parity.None

'theSerialPort.StopBits = StopBits.One

'theSerialPort.DataBits = 8

'theSerialPort.Handshake = Handshake.None

'theSerialPort.RtsEnable = True

Public Class FastSerialPorts

    Public Shared theSerialPort As SerialPort = Nothing

    Private Shared serialPortThread As Thread

    Private Shared canContinue As Boolean = True

    Private Shared isAutoConnect As Boolean = False

    Public Sub setAutoReConnect()
        isAutoConnect = True
    End Sub

    Public Shared Function listPortNames() As String()
        Dim listResult As String() = New String() {}

        Dim i As Integer = 0

        For Each sp As String In My.Computer.Ports.SerialPortNames

            listResult(i) = sp

            i += 1

        Next

        Return listResult
    End Function

    Public Sub connectTo(inputPortName As String)
        canContinue = True

        Try

            theSerialPort = My.Computer.Ports.OpenSerialPort(inputPortName)

            theSerialPort.ReadTimeout = 3000

            theSerialPort.WriteTimeout = 3000

            theSerialPort.BaudRate = 9600

            theSerialPort.Parity = Parity.None

            theSerialPort.StopBits = StopBits.One

            theSerialPort.DataBits = 8

            theSerialPort.Handshake = Handshake.None

            theSerialPort.RtsEnable = True


            serialPortThread = New Thread(New ThreadStart(AddressOf readData))

            serialPortThread.Start()
        Catch ex As Exception
            Throw New Exception("Error: Serial Port read timed out.")

            If isAutoConnect = True Then
                Try
                    tryToConnect()
                Catch subEx As Exception
                    tryToConnect()
                End Try
            End If
        Finally
            If theSerialPort IsNot Nothing Then theSerialPort.Close()
        End Try
    End Sub

    Private Sub tryToConnect()
        Try
            theSerialPort.Open()
        Catch ex As Exception
            tryToConnect()
        End Try
    End Sub

    Private Shared Sub readData()

        Dim Incoming As String

        While (canContinue)
            Incoming = theSerialPort.ReadLine()
            If Incoming Is Nothing Then
                'do action

            Else
                'do action

            End If
        End While

    End Sub

    Public Sub sendData(inputStr As String)
        Try
            theSerialPort.WriteLine(inputStr)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub closePort()

        canContinue = False

        serialPortThread.Abort()

        theSerialPort.Close()

    End Sub

End Class
