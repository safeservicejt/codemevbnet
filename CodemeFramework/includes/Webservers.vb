﻿Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Threading
Imports System.Xml
Imports System.Windows.Forms
Imports System.Collections.Generic
Imports System.Collections
Imports CodemeFramework.includes

Namespace includes

    Class Webservers
        'Dim ws As Webservers = Webservers.getWebServer
        'ws.StartWebServer("C:\wamp\www\")

#Region "Declarations"
        'Dim _listener As HttpListener = New HttpListener()
        'Dim _request As HttpListenerRequest

        'Dim _responderMethod As System.Func(Of HttpListenerRequest, String)

        ''Webservers ws = new Webservers(Webservers.SendResponse, "http://localhost:8080/test/");
        ''ws.Run();
        ''Console.WriteLine("A simple webserver. Press a key to quit.");
        ''Console.ReadKey();
        ''ws.Stop();

        'Public Sub Webservers()

        '    Dim prefixes As String() = {"http://localhost:8080/test/"}

        '    If Not HttpListener.IsSupported Then
        '        Throw New NotSupportedException(
        '            "Needs Windows XP SP2, Server 2003 or later.")
        '    End If

        '    '// URI prefixes are required, for example 
        '    '// "http://localhost:8080/index/".
        '    If (prefixes.Length = 0) Then
        '        Throw New ArgumentException("prefixes")
        '    End If

        '    '// A responder method is required
        '    'If (method = Nothing) Then
        '    '    Throw New ArgumentException("method")

        '    'End If

        '    For Each s As String In prefixes
        '        _listener.Prefixes.Add(s)
        '    Next

        '    '_responderMethod = method
        '    _listener.Start()
        'End Sub


        'Public Sub Run()
        '    ThreadPool.QueueUserWorkItem(Function(o As Object)
        '                                     'Console.WriteLine("Webserver running...");
        '                                     Try
        '                                         _listener.Start()
        '                                         While _listener.IsListening
        '                                             ThreadPool.QueueUserWorkItem(Function(c As Object)
        '                                                                              Dim ctx As HttpListenerContext = TryCast(c, HttpListenerContext)
        '                                                                              Try
        '                                                                                  'Dim rstr As String = _responderMethod(ctx.Request)
        '                                                                                  Dim rstr As String = SendResponse(ctx.Request)
        '                                                                                  Dim buf As Byte() = Encoding.UTF8.GetBytes(rstr)
        '                                                                                  ctx.Response.ContentLength64 = buf.Length
        '                                                                                  ctx.Response.OutputStream.Write(buf, 0, buf.Length)
        '                                                                              Catch
        '                                                                              Finally
        '                                                                                  ' suppress any exceptions
        '                                                                                  ' always close the stream
        '                                                                                  ctx.Response.OutputStream.Close()
        '                                                                              End Try
        '                                                                              Return c
        '                                                                          End Function, _listener.GetContext())
        '                                         End While
        '                                     Catch
        '                                         ' suppress any exceptions
        '                                     End Try
        '                                     Return o
        '                                 End Function)
        'End Sub

        'Public Sub stopServer()
        '    _listener.Stop()
        '    _listener.Close()
        'End Sub

        ''Public Shared Function SendResponse(request As HttpListenerRequest) As String
        ''    Dim rawUrl As String = request.RawUrl
        ''    Dim query As String = request.QueryString("q")

        ''    Return String.Format("<HTML><BODY>My web page.<br>{0}</BODY></HTML>", DateTime.Now)
        ''End Function

        'Public Function SendResponse(request As HttpListenerRequest) As String
        '    Dim rawUrl As String = request.RawUrl
        '    Dim query As String = request.QueryString("q")

        '    Return String.Format("<HTML><BODY>My web page.<br>{0}</BODY></HTML>", DateTime.Now)
        'End Function

        Private Shared singleWebserver As Webservers
        Private Shared blnFlag As Boolean

        Private LocalTCPListener As TcpListener
        Private LocalPort As Integer = 80
        Private LocalAddress As IPAddress = GetIPAddress()
        Private DefaultDoc As String = "index.html"
        Private WebThread As Thread
        Private LocalImageDir As String
        Private LocalVirtualRoot As String

        Public Shared listGetRequest As Dictionary(Of String, String) = New Dictionary(Of String, String)()

        Public Shared requestUri As String = ""

        Private Shared onlyCatchRequest As Integer = 0
#End Region

#Region "Properties"
        Public Property ListenWebPort() As Integer
            Get
                Return LocalPort
            End Get
            Set(ByVal Value As Integer)
                LocalPort = Value
            End Set
        End Property

        Public ReadOnly Property ListenIPAddress() As IPAddress
            Get
                Return LocalAddress
            End Get
        End Property


        Public Property DefaultDocument() As String
            Get
                Return DefaultDoc
            End Get
            Set(ByVal Value As String)
                DefaultDoc = Value
            End Set
        End Property

        Public Property ImageDirectory() As String
            Get
                Return LocalImageDir
            End Get
            Set(ByVal Value As String)
                LocalImageDir = Value
            End Set
        End Property

        Public Property VirtualRoot() As String
            Get
                Return LocalVirtualRoot
            End Get
            Set(ByVal Value As String)
                LocalVirtualRoot = Value
            End Set
        End Property
#End Region

#Region "Methods"

        Private Function GetIPAddress() As IPAddress
            Dim oAddr As System.Net.IPAddress
            Dim sAddr As String
            With System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName())
                If .AddressList.Length > 0 Then
                    oAddr = New IPAddress(.AddressList.GetLowerBound(0))
                End If
            End With
            GetIPAddress = oAddr
        End Function


        Friend Shared Function getWebServer() As Webservers
            If Not blnFlag Then
                singleWebserver = New Webservers
                blnFlag = True
                Return singleWebserver
            Else
                Return singleWebserver
            End If
        End Function


        Public Sub StartWebServer(rootPath As String)

            LocalVirtualRoot = rootPath

            Try
                LocalTCPListener = New TcpListener(LocalAddress, LocalPort)
                LocalTCPListener.Start()
                WebThread = New Thread(AddressOf StartListen)
                WebThread.Start()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
        End Sub
        'Here is where we check our XML file and see what MIME types are defined and handle the accordingly.

        Public Function GetMimeType(ByVal sRequestFile As String) As String
            Dim sr As StreamReader
            Dim sLine As String = ""
            Dim sMimeType As String = ""
            Dim sFileExt As String = ""
            Dim sMimeExt As String = ""
            sRequestFile = sRequestFile.ToLower
            Dim iStartPos As Integer = sRequestFile.IndexOf(".") + 1
            sFileExt = sRequestFile.Substring(iStartPos)

            Try
                'now go through the mime definitions and apply to the request.
                Dim dom As New XmlDocument
                dom.Load(Application.StartupPath & "\mimetypes.xml")
                Dim objCurrentNode As XmlNode
                objCurrentNode = dom.SelectSingleNode("//mimetypes")
                'now go through all child nodes.
                If objCurrentNode.HasChildNodes Then
                    'loop
                    Dim xmlMimeType As XmlNode
                    For Each xmlMimeType In objCurrentNode
                        sMimeExt = xmlMimeType.Name
                        sMimeType = xmlMimeType.InnerText
                        If (sMimeExt = sFileExt) Then
                            Exit For
                        End If
                    Next
                End If
                If sMimeExt = sFileExt Then
                    Return sMimeType
                Else
                    Return ""
                End If

            Catch ex As Exception
                sMimeExt = "html"

                sMimeType = "text/html"
            End Try
        End Function

        Public Function GetTheDefaultFileName(ByVal sLocalDirectory As String) As String
            Return "index.html"
        End Function

        Public Function GetLocalPath(ByVal sWebServerRoot As String, ByVal sDirName As String) As String
            'Dim sr As StreamReader
            'Dim sLine As String = ""
            Dim sVirtualDir As String = ""
            Dim sRealDir As String = ""
            Dim iStartPos As Integer = 0
            sDirName.Trim()
            sWebServerRoot = sWebServerRoot.ToLower
            sDirName = sDirName.ToLower
            Select Case sDirName
                Case "/"
                    sRealDir = LocalVirtualRoot
                Case Else
                    If Mid$(sDirName, 1, 1) = "/" Then
                        sDirName = Mid$(sDirName, 2, Len(sDirName))
                    End If
                    sRealDir = LocalVirtualRoot & sDirName.Replace("/", "\")
            End Select
            Return sRealDir
        End Function

        Public Sub SendHeader(ByVal sHttpVersion As String, ByVal sMimeHeader As String, _
                  ByVal iTotalBytes As Integer, ByVal sStatusCode As String, ByRef thisSocket As Socket)
            Dim sBuffer As String = ""
            If Len(sMimeHeader) = 0 Then
                sMimeHeader = "text/html"
            End If
            sBuffer = sHttpVersion & sStatusCode & vbCrLf & _
                "Server: X10CamControl" & vbCrLf & _
                "Content-Type: " & sMimeHeader & vbCrLf & _
                "Accept-Ranges: bytes" & vbCrLf & _
                "Content-Length: " & iTotalBytes & vbCrLf & vbCrLf

            Dim bSendData As [Byte]() = Encoding.ASCII.GetBytes(sBuffer)
            SendToBrowser(bSendData, thisSocket)
        End Sub

        Public Overloads Sub SendToBrowser(ByVal sData As String, ByRef thisSocket As Socket)
            SendToBrowser(Encoding.ASCII.GetBytes(sData), thisSocket)
        End Sub

        Public Overloads Sub SendToBrowser(ByVal bSendData As [Byte](), ByRef thisSocket As Socket)
            Dim iNumBytes As Integer = 0
            If thisSocket.Connected Then
                If (thisSocket.Send(bSendData, bSendData.Length, 0)) = -1 Then
                    'socket error can't send packet
                Else
                    'number of bytes sent.
                End If
            Else
                'connection dropped.
            End If
        End Sub

        Private Sub New()
            'create a singleton
        End Sub


        Private Sub StartListen()
            Dim iStartPos As Integer
            Dim sRequest As String
            Dim sDirName As String
            Dim sRequestedFile As String
            Dim sErrorMessage As String
            Dim sLocalDir As String
            Dim sWebserverRoot As String = LocalVirtualRoot
            Dim sQueryString As String
            Dim sPhysicalFilePath As String = ""
            Dim sFormattedMessage As String = ""

            Do While True
                'accept new socket connection
                Dim mySocket As Socket = LocalTCPListener.AcceptSocket
                If mySocket.Connected Then
                    Dim bReceive() As Byte = New Byte(1024) {}
                    Dim i As Integer = mySocket.Receive(bReceive, bReceive.Length, 0)
                    Dim sBuffer As String = Encoding.ASCII.GetString(bReceive)
                    'find the GET request.
                    If (sBuffer.Substring(0, 3) <> "GET") Then
                        mySocket.Close()
                        Return
                    End If
                    iStartPos = sBuffer.IndexOf("HTTP", 1)
                    Dim sHttpVersion As String = sBuffer.Substring(iStartPos, 8)
                    sRequest = sBuffer.Substring(0, iStartPos - 1)
                    sRequest.Replace("\\", "/")
                    If (sRequest.IndexOf(".") < 1) And (Not (sRequest.EndsWith("/"))) Then
                        sRequest = sRequest & "/"
                    End If

                    If sRequest.IndexOf("?") <> -1 Then
                        Try
                            parseRequest(sRequest)
                        Catch ex As Exception
                            sErrorMessage = ex.Message
                            SendHeader(sHttpVersion.ToString(), "", sErrorMessage.Length, " 404 Not Found", mySocket)
                            SendToBrowser(sErrorMessage, mySocket)
                            mySocket.Close()
                        End Try
                    End If


                    If onlyCatchRequest = 1 Then
                        Try
                            parseGetRequest()

                        Catch ex As Exception
                            sErrorMessage = ex.Message
                            SendHeader(sHttpVersion.ToString(), "", sErrorMessage.Length, " 404 Not Found", mySocket)
                            SendToBrowser(sErrorMessage, mySocket)
                            mySocket.Close()
                        End Try
                    End If

                    'get the file name
                    iStartPos = sRequest.LastIndexOf("/") + 1
                    sRequestedFile = sRequest.Substring(iStartPos)
                    If InStr(sRequest, "?") <> 0 Then
                        iStartPos = sRequest.IndexOf("?") + 1
                        sQueryString = sRequest.Substring(iStartPos)
                        sRequestedFile = Replace(sRequestedFile, "?" & sQueryString, "")
                    End If
                    'get the directory
                    sDirName = sRequest.Substring(sRequest.IndexOf("/"), sRequest.LastIndexOf("/") - 3)
                    'identify the physical directory.
                    If (sDirName = "/") Then
                        sLocalDir = sWebserverRoot
                    Else
                        sLocalDir = GetLocalPath(sWebserverRoot, sDirName)
                    End If
                    'if the directory isn't there then display error.
                    If sLocalDir.Length = 0 Then
                        sErrorMessage = "Error!! Requested Directory does not exists"
                        SendHeader(sHttpVersion.ToString(), "", sErrorMessage.Length, " 404 Not Found", mySocket)
                        SendToBrowser(sErrorMessage, mySocket)
                        mySocket.Close()
                    End If

                    If sRequestedFile.Length = 0 Then
                        sRequestedFile = GetTheDefaultFileName(sLocalDir)
                        If sRequestedFile = "" Then
                            sErrorMessage = "Error!! No Default File Name Specified"
                            SendHeader(sHttpVersion.ToString(), "", sErrorMessage.Length, " 404 Not Found", mySocket)
                            SendToBrowser(sErrorMessage, mySocket)
                            mySocket.Close()
                            Return
                        End If
                    End If

                    Dim sMimeType As String = GetMimeType(sRequestedFile)
                    sPhysicalFilePath = sLocalDir & sRequestedFile
                    If Not File.Exists(sPhysicalFilePath) Then
                        sErrorMessage = "404 Error! File Does Not Exists..."
                        SendHeader(sHttpVersion.ToString(), "", sErrorMessage.Length, " 404 Not Found", mySocket)
                        SendToBrowser(sErrorMessage, mySocket)
                    Else

                        Try
                            Dim iTotBytes As Integer = 0
                            Dim sResponse As String = ""
                            Dim fs As New FileStream(sPhysicalFilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
                            Dim reader As New BinaryReader(fs)
                            Dim bytes() As Byte = New Byte(Convert.ToInt32(fs.Length)) {}

                            While reader.BaseStream.Position < reader.BaseStream.Length
                                reader.Read(bytes, 0, bytes.Length)
                                sResponse = sResponse & Encoding.ASCII.GetString(bytes, 0, Convert.ToInt32(reader.BaseStream.Length))
                                iTotBytes = Convert.ToInt32(reader.BaseStream.Length)
                            End While
                            reader.Close()
                            fs.Close()
                            SendHeader(sHttpVersion.ToString(), sMimeType, iTotBytes, " 200 OK", mySocket)
                            SendToBrowser(bytes, mySocket)
                        Catch ex As Exception
                            sErrorMessage = "404 Error! File Does Not Exists..."
                            SendHeader(sHttpVersion.ToString(), "", sErrorMessage.Length, " 404 Not Found", mySocket)
                            SendToBrowser(sErrorMessage, mySocket)
                        End Try

                    End If
                    mySocket.Close()

                End If
            Loop

        End Sub

        Private Sub parseRequest(inputStringReg As String)
            Dim strSplit As String() = inputStringReg.Split(New String() {"?"}, StringSplitOptions.None)

            requestUri = strSplit(1)

            Dim splitReg As String() = requestUri.Split(New String() {"&"}, StringSplitOptions.None)

            Dim totalReq As Integer = splitReg.Length

            Dim theKey As String = ""

            Dim theVal As String = ""

            For Each req As String In splitReg

                theKey = req.Substring(0, req.IndexOf("="))

                theVal = req.Substring(req.IndexOf("=") + 1)

                'base64:the base64 value

                If theVal.IndexOf("base64:") <> -1 Then
                    Try
                        theVal = Strings.base64_decode(theVal.Substring(theVal.IndexOf("base64:") + 7))
                    Catch ex As Exception
                        theVal = "NG"
                    End Try
                End If

                If Not listGetRequest.ContainsKey(theKey) Then
                    listGetRequest.Add(theKey, theVal)
                Else
                    listGetRequest.Item(theKey) = theVal
                End If

            Next

        End Sub

        Public Shared Sub catchGetRequest()
            onlyCatchRequest = 1
        End Sub

        Private Shared Sub parseGetRequest()

            If listGetRequest.ContainsKey("get") Then
                If listGetRequest.Item("get") = "ac" Then
                    Throw New Exception("fdfdf")
                End If
            ElseIf listGetRequest.ContainsKey("set") Then

            ElseIf listGetRequest.ContainsKey("do") Then

            End If

        End Sub


        Public Sub StopWebServer()
            Try
                LocalTCPListener.Stop()
                WebThread.Abort()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
        End Sub
#End Region




    End Class
End Namespace

