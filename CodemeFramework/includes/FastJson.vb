Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports Newtonsoft.Json


Namespace includes
	Class FastJson
		Public Shared Function serializeDictionary(points As Dictionary(Of String, String)) As String
			'Dictionary<string, int> points = new Dictionary<string, int>
			'{
			'    { "James", 9001 },
			'    { "Jo", 3474 },
			'    { "Jess", 11926 }
			'};

			Dim json As String = JsonConvert.SerializeObject(points, Formatting.Indented)

			Return json
		End Function

		Public Shared Function serializeList(points As List(Of String)) As String
			'List<string> videogames = new List<string>
			'{
			'    "Starcraft",
			'    "Halo",
			'    "Legend of Zelda"
			'};

			Dim json As String = JsonConvert.SerializeObject(points)

			Return json
		End Function


	End Class
End Namespace
