Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Threading

Namespace includes
	Public Class Images

		Public Shared Sub addWaterMark(fromImage As String, wtmImage As String, savePath As String, Optional posX As Integer = 0, Optional poxY As Integer = 0)
			Using image__1 As Image = Image.FromFile(fromImage)
				Using watermarkImage As Image = Image.FromFile(wtmImage)
					Using imageGraphics As Graphics = Graphics.FromImage(image__1)
						Using watermarkBrush As New TextureBrush(watermarkImage)
							Dim x As Integer = posX
							Dim y As Integer = poxY
							watermarkBrush.TranslateTransform(x, y)
							imageGraphics.FillRectangle(watermarkBrush, New Rectangle(New Point(x, y), New Size(watermarkImage.Width + 1, watermarkImage.Height)))
							image__1.Save(savePath)
						End Using
					End Using
				End Using
			End Using
		End Sub

		Public Shared Sub addWaterMarkCenter(fromImage As String, wtmImage As String, savePath As String)
			Using image__1 As Image = Image.FromFile(fromImage)
				Using watermarkImage As Image = Image.FromFile(wtmImage)
					Using imageGraphics As Graphics = Graphics.FromImage(image__1)
						Using watermarkBrush As New TextureBrush(watermarkImage)
							Dim x As Integer = (image__1.Width \ 2 - watermarkImage.Width \ 2)
							Dim y As Integer = (image__1.Height \ 2 - watermarkImage.Height \ 2)
							watermarkBrush.TranslateTransform(x, y)
							imageGraphics.FillRectangle(watermarkBrush, New Rectangle(New Point(x, y), New Size(watermarkImage.Width + 1, watermarkImage.Height)))
							image__1.Save(savePath)
						End Using
					End Using
				End Using
			End Using
		End Sub

		Public Shared Sub cropCenterImage(Width As Integer, Height As Integer, sourceFilePath As String, saveFilePath As String)
			'cropCenterImage(100, 100, "c://destinationimage.jpg", "c://newcroppedimage.jpg");

			' variable for percentage resize 
			Dim percentageResize As Single = 0
			Dim percentageResizeW As Single = 0
			Dim percentageResizeH As Single = 0

			' variables for the dimension of source and cropped image 
			Dim sourceX As Integer = 0
			Dim sourceY As Integer = 0
			Dim destX As Integer = 0
			Dim destY As Integer = 0

			' Create a bitmap object file from source file 
			Dim sourceImage As New Bitmap(sourceFilePath)

			' Set the source dimension to the variables 
			Dim sourceWidth As Integer = sourceImage.Width
			Dim sourceHeight As Integer = sourceImage.Height

			' Calculate the percentage resize 
			percentageResizeW = (CSng(Width) / CSng(sourceWidth))
			percentageResizeH = (CSng(Height) / CSng(sourceHeight))

			' Checking the resize percentage 
			If percentageResizeH < percentageResizeW Then
				percentageResize = percentageResizeW
				destY = System.Convert.ToInt16((Height - (sourceHeight * percentageResize)) / 2)
			Else
				percentageResize = percentageResizeH
				destX = System.Convert.ToInt16((Width - (sourceWidth * percentageResize)) / 2)
			End If

			' Set the new cropped percentage image
			Dim destWidth As Integer = CInt(Math.Truncate(Math.Round(sourceWidth * percentageResize)))
			Dim destHeight As Integer = CInt(Math.Truncate(Math.Round(sourceHeight * percentageResize)))

			' Create the image object 
			Using objBitmap As New Bitmap(Width, Height)
				objBitmap.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution)
				Using objGraphics As Graphics = Graphics.FromImage(objBitmap)
					' Set the graphic format for better result cropping 
					objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias
					objGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
					objGraphics.DrawImage(sourceImage, New Rectangle(destX, destY, destWidth, destHeight), New Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel)

					' Save the file path, note we use png format to support png file 
					objBitmap.Save(saveFilePath, ImageFormat.Png)
				End Using
			End Using
		End Sub
		Public Function imageSize(filePath As String) As Integer()
			Dim result As Integer() = {}

			Dim img As System.Drawing.Image = System.Drawing.Image.FromFile(filePath)

			result(0) = img.Width
			result(1) = img.Height

			Return result
		End Function

		Public Function resizeImage(newWidth As Integer, newHeight As Integer, stPhotoPath As String) As Image
			Dim imgPhoto As Image = Image.FromFile(stPhotoPath)

			Dim sourceWidth As Integer = imgPhoto.Width
			Dim sourceHeight As Integer = imgPhoto.Height

			'Consider vertical pics
			If sourceWidth < sourceHeight Then
				Dim buff As Integer = newWidth

				newWidth = newHeight
				newHeight = buff
			End If

			Dim sourceX As Integer = 0, sourceY As Integer = 0, destX As Integer = 0, destY As Integer = 0
			Dim nPercent As Single = 0, nPercentW As Single = 0, nPercentH As Single = 0

			nPercentW = (CSng(newWidth) / CSng(sourceWidth))
			nPercentH = (CSng(newHeight) / CSng(sourceHeight))
			If nPercentH < nPercentW Then
				nPercent = nPercentH
				destX = System.Convert.ToInt16((newWidth - (sourceWidth * nPercent)) / 2)
			Else
				nPercent = nPercentW
				destY = System.Convert.ToInt16((newHeight - (sourceHeight * nPercent)) / 2)
			End If

			Dim destWidth As Integer = CInt(Math.Truncate(sourceWidth * nPercent))
			Dim destHeight As Integer = CInt(Math.Truncate(sourceHeight * nPercent))


			Dim bmPhoto As New Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb)

			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution)

			Dim grPhoto As Graphics = Graphics.FromImage(bmPhoto)
			grPhoto.Clear(Color.Black)
			grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic

			grPhoto.DrawImage(imgPhoto, New Rectangle(destX, destY, destWidth, destHeight), New Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel)

			grPhoto.Dispose()
			imgPhoto.Dispose()
			Return bmPhoto
		End Function

	End Class
End Namespace
